import json
import os
import pathlib

basedir = pathlib.Path(__file__).parent.resolve()

try:
    mapping = json.loads(os.environ["YAOOK_INTERSPHINX"])
except KeyError:
    mapping = {
        "yaook_installation_guide": (
            str(basedir / "src" / "_build" / "html"), None,
        ),
    }

