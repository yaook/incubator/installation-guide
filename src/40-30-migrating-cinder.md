# Migrating Cinder
The following guide will show you how to migrate cinder to YAOOK with minimal downtime.
Other openstack services might be running as YAOOK services already or might still live outside YAOOK.

## Downtime Impact

When following this guide you will have a downtime for changes (volume, snapshot, backup creation and deletion) on the cinder api depending on the size of your environment.
You will also be unable to attach and detach volumes during this time.

You should ensure that no backups in cinder-backup are running at the start of the migration.
Running backups might get stuck in their current state and would need a manual cleanup.

It is reasonable to finish this migration in 2-3 hours.
Of this duration the downtime can be as low as 30 min if everything is prepared.

## Requirements

Please ensure the following before following this guide:
* Ensure you have a ready YAOOK setup with operators and their dependencies up and running

## Preparations

### Preparations of dependent services

Other openstack services can rely on the cinder api to attach and detach volumes (especially nova or heat).
As the internal endpoint of cinder will be inside the YAOOK Kubernetes cluster it will no longer be reachable from the outside.
Therefor you might need to change the dependent services to use the `public` endpoint.

Also you will need to update the static keystone `auth_url` in your settings during the migration.
Already prepare the necessary changes for that so that these can be rolled out easily.
Note that some openstack services (e.g. neutron) have multiple sections where the `auth_url` might be configured.

### Prepare a migration policy setting

During the migration you will need to prevent updates to cinder.
The easiest way to do this is to update the policy of the service and deny all changes.
As the policy differs for each deployment please already prepare such a policy in advance.

### Prepare CinderDeployment

Prepare a `CinderDeployment` resource with the options you want to have set.
You can find documentation for the resource [here](https://docs.yaook.cloud/devel/examples/cinder.html).

If you are using a NFS based storage backend you probably need to set `.spec.ids.uid` and `.spec.ids.gid` to the ids of your old cinder user.

### Prepare storage backend

Depending on your storage backend you might need to adopt settings to allow access from different source ips (e.g. for NFS).

## Migration

A part time of the migration can already be prepared without a downtime.
The downtime start will be explicitly mentioned below.

### Installation of Cinder

Installing a parallel cinder is already possible if you are careful that the existing keystone endpoint is not overwritten.

1. Stop the keystone-resource-operator by scaling its deployment to 0 and wait for it to terminate
2. Apply the `CinderDeployment` you created above and wait for it to depend on the keystone user
3. Stop the cinder-operator by scaling its deployment to 0 and wait for it to terminate
4. Delete the cinder endpoint in the `KeystoneEndpoint` resource
5. Start the keystone-resource-operator by scaling its deployment to 1
6. Wait for the cinder `KeystoneUser` to reconcile successfully
7. Stop the keystone-resource-operator by scaling its deployment to 0 and wait for it to terminate
8. Start the cinder-operator by scaling its deployment to 1
9. Wait for the `CinderDeployment` to reconcile successfully
10. Update the `CinderDeployment` policy setting to have your migration policy from above included
11. Wait for the reconcile to complete

### Migration of Cinder

Starting here these steps will have the above described Downtime.
Please plan accordingly.

1. Update the cinder policy on your existing cinder environment to the migration policy defined above.
   This will prevent further modifications and therefor we can leave the api running.
2. Restart the cinder api services afterwards
3. Export the cinder database using `mysqldump`
4. Copy the dump to the first cinder database container in k8s using `kubectl cp ...`
5. Get the cinder db admin user pw from the secret `cinder-**-db-creds` and the key `mariadb-root-password` in there
6. open a shell on the first cinder database pod in the `mariadb-galera` container
7. Load the db dump again using `mysql -u yaook-sys-maint cinder -p < cinder_cleaned.dump` and enter the password when prompted
8. If you now `curl` the new cinder ingress endpoint you should get a valid response
    - Get yourself a keystone token using `openstack token issue` and save the id
    - in a new shell without ANY OpenStack environment variables set, set the following
        - `OS_TOKEN` to the value of the token from above
        - `OS_ENDPOINT` to the url of cinder including the version (e.g. `https://cinder.example.com/v3/%(project_id)s`)
    - now a `openstack volume list` should return the expected result
12. Start the keystone-resource-operator by scaling its deployment to 1
13. Wait for the "KeystoneEndpoint" resource to reconcile correctly
14. At this point the yaook cinder will be active.

### Migration of dependent services

As described above you might need to change the configuration of other openstack services to use the new cinder api.
If you need to do this, now is the appropriate time.

### Cleaning up

At this point nobody should be using the old cinder anymore.
You can validate that on the old cinder api logs.
If you still see requests you should clean them up now.

Afterwards:
- Stop the old cinder api
- revert your policy to whatever your original setting is

The migration should now be finished.
You can now also clean up everything related to the old cinder service.
