# Nova and Neutron

The Nova and Neutron services have to be deployed in lockstep, as there is a cross-dependency between them: Nova requires Neutron to spawn compute services, but Neutron requires a Nova deployment in order to configure the metadata proxies.

As Neutron needs to know about Nova for configuration purposes and Nova only has a soft dependency on Neutron (it can run just fine with 0 compute nodes, albeit also rather useless), we start with Nova.

**Note:** As of the time of writing, YAOOK does not support Nova setups with more than one cell.

**Note:** Placement, even though a separate upstream service these days, is fully integrated in the YAOOK nova operator.

## Nova

### Nova itself

The resource to deploy Nova is a NovaDeployment. The skeleton of the resource looks like this:

```yaml
apiVersion: yaook.cloud/v1
kind: NovaDeployment
metadata:
    name: nova
    namespace: yaook
spec:
    ..
```

The full reference for fields available on the `NovaDeployment` is available
in the
[API reference documentation](https://docs.yaook.cloud/handbook/api-reference.html#user-api-reference-novadeployment).

Nova is more complex than the services we encountered so far, with four databases, one message queue, a whopping seven nova-specific components and of course the nova-compute configuration. This custom resource has, under `spec`, the following keys:

* `spec.targetRelease` to set the OpenStack release to deploy.
* `spec.keystoneRef.name` needs to be set to the name of your KeystoneDeployment.
* `spec.api.ingress.fqdn` and `spec.api.ingress.port`, to make the external Nova endpoints reachable.
* `spec.placement.ingress.fqdn` and `spec.placement.ingress.port`, to make the external Placement endpoints reachable.
* `spec.database.*.storageClassName` to ensure the database volumes use the correct storage class.
* `spec.database.*.storageSize` to ensure the database volumes has sufficient space.
* `spec.messageQueue.*.storageClassName` to ensure the rabbitmq volumes use the correct storage class.
* `spec.issuerRef.name` to make YAOOK able to issue certificates for internal use; should be set to `yaook-internal` if you have been following this guide.
* `spec.region.name` to consistently set the region throughout your OpenStack deployment.
* `spec.compute` for nova-compute configuration. If you just want to set system-wide configuration, you can add a configTemplate with `matchLabels: {}`. Otherwise, you can use this powerful tool to set per-node configuration based on Kubernetes labels.

    **Note:** The [YAOOK rules of "no config overwrites"](https://docs.yaook.cloud/concepts/configuration.html) apply. That means that if you set e.g. `DEBUG: "true"` on `foo=a` and `DEBUG: "false"` on `bar=a`, then the operator will fail to generate the configuration for a node with `foo=a,bar=a`, as two conflicting values are present in the configuration source for such a node.

    **Note:** To apply some defaults/common values for nova-compute on all compute nodes, use `nodeSelectors: matchLabels: {}`. This will *not* cause nova-compute to *spawn* on all nodes; that is still controlled by the `compute.yaook.cloud/hypervisor` scheduling key.

### Host Aggregates

Host aggregates in OpenStack can be managed by YAOOK. This is not mandatory; you can continue to manually manage the host aggregates. If you *do* let YAOOK manage these, they can be taken into account during rolling upgrades, to avoid breaking too many nodes from a single aggregate at the same time.

Aggregates are configured using the `NovaHostAggregate` custom resource:

```yaml
apiVersion: compute.yaook.cloud/v1
kind: NovaHostAggregate
metadata:
    name: <insert name>
    namespace: yaook
spec:
    novaRef:
        name: <insert name of nova deployment here>
    keystoneRef:
        name: <insert name of keystone deployment here>
    zone: <insert name of availability zone here, optional>
    properties: {
        <additional properties as desired>
    }
```

The full reference for fields available on the `NovaHostAggregate` is available
in the
[API reference documentation](https://docs.yaook.cloud/handbook/api-reference.html#user-api-reference-novahostaggregate).

HostAggregates get assigned to Nodes like the following:

1. The k8s Node needs a custom label, selected for all nodes that should have
    the aggregate.
1. At novadeployment, configure the section
    `spec.compute.configTemplates` with `nodeSelectors` selecting the defined
    label and `hostAggregates` having the name of your `NovaHostAggregate`

## Neutron

The resource to deploy Neutron is a NeutronDeployment. The skeleton of the resource looks like this:

```yaml
apiVersion: yaook.cloud/v1
kind: NeutronDeployment
metadata:
    name: neutron
    namespace: yaook
spec:
    ..
```

The full reference for fields available on the `NeutronDeployment` is available
in the
[API reference documentation](https://docs.yaook.cloud/handbook/api-reference.html#user-api-reference-neutrondeployment).

The most important keys you'll have to think about are:

* `spec.targetRelease` to set the OpenStack release to deploy.
* `spec.keystoneRef.name` needs to be set to the name of your KeystoneDeployment.
* `spec.api.ingress.fqdn` and `spec.api.ingress.port`, to make the external Neutron endpoints reachable.
* `spec.database.storageClassName` to ensure the database volumes use the correct storage class.
* `spec.database.storageSize` to ensure the database volumes has sufficient space.
* `spec.messageQueue.storageClassName` to ensure the rabbitmq volumes use the correct storage class.
* `spec.issuerRef.name` to make YAOOK able to issue certificates for internal use; should be set to `yaook-internal` if you have been following this guide
* `spec.region.name` to consistently set the region throughout your OpenStack deployment.
* `spec.setup.ovn` to configure the layer 2 and layer 3 infrastructure.

Once you have built your NeutronDeployment resource (you may use the [official NeutronDeployment example as reference](https://docs.yaook.cloud/examples/neutron.html), though be careful that its values are not safe for production!), you can apply it with kubectl into the yaook namespace.

After that, you can watch the progress by calling `yaookctl status openstack`.

Once the Neutron entry in `yaookctl status openstack` has reached `Success` state, you can use `yaookctl shell openstack` to get a shell which has OpenStack credentials loaded. In that shell, you can run `openstack network agent list`; This should list at least all compute nodes and all network nodes.

With Neutron and Nova deployed, it should now be possible to spawn entire VMs!
