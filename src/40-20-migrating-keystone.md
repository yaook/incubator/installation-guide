# Migrating Keystone
The following guide will show you how to migrate keystone to YAOOK with minimal downtime.
Other openstack services might be running as YAOOK services already or might still live outside YAOOK.

## Downtime Impact

When following this guide you will have a downtime for changes (user, project creation and deletion) on the keystone api depending on the size of your environment.
It is reasonable to finish this migration in 2-3 hours.
Of this duration the downtime can be as low as 30 min if everything is prepared.
The authentication of existing users will work during the whole migration time

## Requirements

Please ensure the following before following this guide:
* Ensure you have a ready YAOOK setup with operators and their dependencies up and running

## Preparations

(preparations-of-dependent-services)=
### Preparation of your old keystone

During the migration we need to migrate the fernet and credential keys of your old keystone to the new one.
This ensures that tokens issued on one of them continue to work on the other as well.
If you have some kind of automatic rotation of these keys enabled please disable it now.

### Preparations of dependent services

All other openstack services rely on the keystone endpoint for authentication and their own operations.
As the internal endpoint of keystone will be inside the YAOOK Kubernetes cluster it will no longer be reachable from the outside.
Therefor you might need to change the dependent services to use the `public` endpoint.

Also you will need to update the static keystone `auth_url` in your settings during the migration.
Already prepare the necessary changes for that so that these can be rolled out easily.
Note that some openstack services (e.g. neutron) have multiple sections where the `auth_url` might be configured.

### Prepare a migration policy setting

During the migration you will need to prevent writes to keystone.
The easiest way to do this is to update the policy of the service and deny all changes.
As the policy differs for each deployment please already prepare such a policy in advance.
You might want to use the following policy as a reference:

<details>
<summary>Keystone Policy</summary>

```
"service_role": "role:service"
"service_or_admin": "rule:admin_required or rule:service_role"
"owner": "user_id:%(user_id)s"
"admin_or_owner": "rule:admin_required or rule:owner"
"token_subject": "user_id:%(target.token.user_id)s"
"admin_or_token_subject": "rule:admin_required or rule:token_subject"
"service_admin_or_token_subject": "rule:service_or_admin or rule:token_subject"
"identity:get_application_credential": "rule:admin_or_owner"
"identity:list_application_credentials": "rule:admin_or_owner"
"identity:create_application_credential": "!"
"identity:delete_application_credential": "!"
"identity:authorize_request_token": "rule:admin_required"
"identity:get_access_token": "rule:admin_required"
"identity:get_access_token_role": "rule:admin_required"
"identity:list_access_tokens": "rule:admin_required"
"identity:list_access_token_roles": "rule:admin_required"
"identity:delete_access_token": "!"
"identity:get_auth_catalog": ""
"identity:get_auth_projects": ""
"identity:get_auth_domains": ""
"identity:get_auth_system": ""
"identity:get_consumer": "rule:admin_required"
"identity:list_consumers": "rule:admin_required"
"identity:create_consumer": "!"
"identity:update_consumer": "!"
"identity:delete_consumer": "!"
"identity:get_credential": "rule:admin_required"
"identity:list_credentials": "rule:admin_required"
"identity:create_credential": "!"
"identity:update_credential": "!"
"identity:delete_credential": "!"
"identity:get_domain": "rule:admin_required or token.project.domain.id:%(target.domain.id)s"
"identity:list_domains": "rule:admin_required"
"identity:create_domain": "!"
"identity:update_domain": "!"
"identity:delete_domain": "!"
"identity:create_domain_config": "!"
"identity:get_domain_config": "rule:admin_required"
"identity:get_security_compliance_domain_config": ""
"identity:update_domain_config": "!"
"identity:delete_domain_config": "!"
"identity:get_domain_config_default": "rule:admin_required"
"identity:ec2_get_credential": "rule:admin_required or (rule:owner and user_id:%(target.credential.user_id)s)"
"identity:ec2_list_credentials": "rule:admin_or_owner"
"identity:ec2_create_credential": "!"
"identity:ec2_delete_credential": "!"
"identity:get_endpoint": "rule:admin_required"
"identity:list_endpoints": "rule:admin_required"
"identity:create_endpoint": "!"
"identity:update_endpoint": "!"
"identity:delete_endpoint": "!"
"identity:create_endpoint_group": "!"
"identity:list_endpoint_groups": "rule:admin_required"
"identity:get_endpoint_group": "rule:admin_required"
"identity:update_endpoint_group": "!"
"identity:delete_endpoint_group": "!"
"identity:list_projects_associated_with_endpoint_group": "rule:admin_required"
"identity:list_endpoints_associated_with_endpoint_group": "rule:admin_required"
"identity:get_endpoint_group_in_project": "rule:admin_required"
"identity:list_endpoint_groups_for_project": "rule:admin_required"
"identity:add_endpoint_group_to_project": "!"
"identity:remove_endpoint_group_from_project": "!"
"identity:check_grant": "rule:admin_required"
"identity:list_grants": "rule:admin_required"
"identity:create_grant": "!"
"identity:revoke_grant": "!"
"identity:list_system_grants_for_user": "rule:admin_required"
"identity:check_system_grant_for_user": "rule:admin_required"
"identity:create_system_grant_for_user": "!"
"identity:revoke_system_grant_for_user": "!"
"identity:list_system_grants_for_group": "rule:admin_required"
"identity:check_system_grant_for_group": "rule:admin_required"
"identity:create_system_grant_for_group": "!"
"identity:revoke_system_grant_for_group": "!"
"identity:get_group": "rule:admin_required"
"identity:list_groups": "rule:admin_required"
"identity:list_groups_for_user": "rule:admin_or_owner"
"identity:create_group": "!"
"identity:update_group": "!"
"identity:delete_group": "!"
"identity:list_users_in_group": "rule:admin_required"
"identity:remove_user_from_group": "!"
"identity:check_user_in_group": "rule:admin_required"
"identity:add_user_to_group": "!"
"identity:create_identity_provider": "!"
"identity:list_identity_providers": "rule:admin_required"
"identity:get_identity_provider": "rule:admin_required"
"identity:update_identity_provider": "!"
"identity:delete_identity_provider": "!"
"identity:get_implied_role": "rule:admin_required"
"identity:list_implied_roles": "rule:admin_required"
"identity:create_implied_role": "!"
"identity:delete_implied_role": "!"
"identity:list_role_inference_rules": "rule:admin_required"
"identity:check_implied_role": "rule:admin_required"
"identity:get_limit": ""
"identity:list_limits": ""
"identity:create_limits": "!"
"identity:update_limits": "!"
"identity:delete_limit": "!"
"identity:create_mapping": "!"
"identity:get_mapping": "rule:admin_required"
"identity:list_mappings": "rule:admin_required"
"identity:delete_mapping": "!"
"identity:update_mapping": "!"
"identity:get_policy": "rule:admin_required"
"identity:list_policies": "rule:admin_required"
"identity:create_policy": "!"
"identity:update_policy": "!"
"identity:delete_policy": "!"
"identity:create_policy_association_for_endpoint": "!"
"identity:check_policy_association_for_endpoint": "rule:admin_required"
"identity:delete_policy_association_for_endpoint": "!"
"identity:create_policy_association_for_service": "!"
"identity:check_policy_association_for_service": "rule:admin_required"
"identity:delete_policy_association_for_service": "!"
"identity:create_policy_association_for_region_and_service": "!"
"identity:check_policy_association_for_region_and_service": "rule:admin_required"
"identity:delete_policy_association_for_region_and_service": "!"
"identity:get_policy_for_endpoint": "rule:admin_required"
"identity:list_endpoints_for_policy": "rule:admin_required"
"identity:get_project": "rule:admin_required or project_id:%(target.project.id)s"
"identity:list_projects": "rule:admin_required"
"identity:list_user_projects": "rule:admin_or_owner"
"identity:create_project": "!"
"identity:update_project": "!"
"identity:delete_project": "!"
"identity:list_project_tags": "rule:admin_required or project_id:%(target.project.id)s"
"identity:get_project_tag": "rule:admin_required or project_id:%(target.project.id)s"
"identity:update_project_tags": "!"
"identity:create_project_tag": "!"
"identity:delete_project_tags": "!"
"identity:delete_project_tag": "!"
"identity:list_projects_for_endpoint": "rule:admin_required"
"identity:add_endpoint_to_project": "!"
"identity:check_endpoint_in_project": "rule:admin_required"
"identity:list_endpoints_for_project": "rule:admin_required"
"identity:remove_endpoint_from_project": "!"
"identity:create_protocol": "!"
"identity:update_protocol": "!"
"identity:get_protocol": "rule:admin_required"
"identity:list_protocols": "rule:admin_required"
"identity:delete_protocol": "!"
"identity:get_region": ""
"identity:list_regions": ""
"identity:create_region": "!"
"identity:update_region": "!"
"identity:delete_region": "!"
"identity:get_registered_limit": ""
"identity:list_registered_limits": ""
"identity:create_registered_limits": "!"
"identity:update_registered_limits": "!"
"identity:delete_registered_limit": "!"
"identity:list_revoke_events": "rule:service_or_admin"
"identity:get_role": "rule:admin_required"
"identity:list_roles": "rule:admin_required"
"identity:create_role": "!"
"identity:update_role": "!"
"identity:delete_role": "!"
"identity:get_domain_role": "rule:admin_required"
"identity:list_domain_roles": "rule:admin_required"
"identity:create_domain_role": "!"
"identity:update_domain_role": "!"
"identity:delete_domain_role": "!"
"identity:list_role_assignments": "rule:admin_required"
"identity:list_role_assignments_for_tree": "rule:admin_required"
"identity:get_service": "rule:admin_required"
"identity:list_services": "rule:admin_required"
"identity:create_service": "!"
"identity:update_service": "!"
"identity:delete_service": "!"
"identity:create_service_provider": "!"
"identity:list_service_providers": "rule:admin_required"
"identity:get_service_provider": "rule:admin_required"
"identity:update_service_provider": "!"
"identity:delete_service_provider": "!"
"identity:revocation_list": "rule:service_or_admin"
"identity:check_token": "rule:admin_or_token_subject"
"identity:validate_token": "rule:service_admin_or_token_subject"
"identity:revoke_token": "rule:admin_or_token_subject"
"identity:create_trust": "user_id:%(trust.trustor_user_id)s"
"identity:list_trusts": ""
"identity:list_roles_for_trust": ""
"identity:get_role_for_trust": ""
"identity:delete_trust": ""
"identity:get_trust": ""
"identity:get_user": "rule:admin_or_owner"
"identity:list_users": "rule:admin_required"
"identity:list_projects_for_user": ""
"identity:list_domains_for_user": ""
"identity:create_user": "!"
"identity:update_user": "!"
"identity:delete_user": "!"
```

</details>

### Prepare KeystoneDeployment

Prepare a `KeystoneDeployment` resource with the options you want to have set.
You can find documentation for the resource [here](https://docs.yaook.cloud/examples/keystone.html).

Please take care to size your `KeystoneDeployment` appropriately.
You might need to increase the amount of api pods.
In larger deployments you can also increase the amount of wsgi workers per pod.

## Migration

A part time of the migration can already be prepared without a downtime.
The downtime start will be explicitly mentioned below.

### Installation of Keystone

Installing a parallel keystone is already possible.

1. Apply the `KeystoneDeployment` you created above and wait for a full reconcile
2. Update the `KeystoneDeployment` policy setting to have your migration policy from above included
3. Wait for the reconcile to complete
4. If you want to use the memcached of keystone for your other services as well you will need to expose the keystone memcached now.
   You can do this by creating multiple k8s Services of type `LoadBalancer`, each pointing to a individual memcached.
5. You should be able to use the new keystone now with the `yaook-sys-maint` user that is created automatically.
   You can find the credentials for this user in the secret `keystone-admin`
6. Create the user `yaook-sys-maint` with the same password in your old keystone.
   Ensure it has admin permissions on the `admin` project.
   This saves us from having to patch in the user later in the database dump.

### Migration Preparation

Before we actually start the migration (and therefor the downtime) we can already migrate the fernet and credential keys.

1. Navigate to the fernet key repository on your old keystone deployment
2. Run `for i in *; do echo "\"$i\" : $(echo -n $(cat $i) | base64)"; done` and save the output
3. Search for a secret named `fernet-keys-***` in your kubernetes cluster
4. Update the data in it with the output from step 2 (this will overwrite some values)

Repeat the previous steps for the credential keys.
For these you need to write to a secret named `credential-keys-***`.


### Migration of Keystone

Starting here these steps will have the above described Downtime.
Please plan accordingly.

1. Update the keystone policy on your existing keystone environment to the migration policy defined above.
   This will prevent further modifications and therefor we can leave the api running.
2. Restart the keystone api services afterwards
3. Export the keystone database using `mysqldump`
4. Copy the dump to the first keystone database container in k8s using `kubectl cp ...`
5. Get the keystone db admin user pw from the secret `keystone-**-db-creds` and the key `mariadb-root-password` in there
6. open a shell on the first keystone database pod in the `mariadb-galera` container
7. Load the db dump again using `mysql -u yaook-sys-maint keystone -p < keystone_cleaned.dump` and enter the password when prompted
8. Login to the database interactively using `mysql -u yaook-sys-maint keystone -p` and enter the password
9. Show the url of all keystone endpoints using `select endpoint.id, interface, url from endpoint join service on (service_id = service.id) where service.type="identity";`
10. Update the url of the internal endpoint using `update endpoint set url='http://keystone.yaook.svc:5000' where id=<internal-endpoint-id>;` (replace the internal-endpoint-id)
11. Update the url of the public and admin endpoint using `update endpoint set url='<keystone ingress>' where id=<admin/public-endpoint-id>;` (replace the endpoint-id and replace the `<keystone ingress>` by whatever ingress url you have defined)
12. At this point the yaook keystone api should work in read only mode.
    You can check this by using your normal `openrc` and just changing the `OS_AUTH_URL`.
    Validate that this allows you to talk to other services and e.g. provision a virtual machine
13. If you have other openstack services already running in yaook that refer to some `ExternalKeystoneDeployment` you should now update them to point to the new `KeystoneDeployment`

### Keystone Redirects

To keep the downtime as short as possible we will now add a redirect that lets all services and users use the new keystone instead of the old one.

1. Update your old public endpoint entrypoint to be a reverse proxy to your new keystone
2. Update your old internal endpoint entrypoint to be a reverse proxy to your new keystone

Note that for this proxying to work your might need to set an appropriate host header.

### Validation

1. Verify that the old keystone is no longer receiving any traffic. If it still is you need to update your redirects
2. Verify that using the new keystone you can do all api operations

### End of Migration

1. Stop the old keystone to ensure nothing can get in the way
2. Revert the policy changes in your new keystone to enable writes to it again.

At this point the Downtime has ended.

## Cleanup

You can now update all of your non-yaook services to directly point to the new keystone instead of relying on the redirect.
Use the changes you have prepared [above](#preparations-of-dependent-services) for this.

After that is done you should be able to remove the redirect for your internal endpoint.

The migration should now be finished.
