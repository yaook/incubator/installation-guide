# Installing the Management Kubernetes

## Installation using k3s

For development setups, k3s can be used to provide a Kubernetes cluster for the management plane. Please refer to the [k3s documentation](https://k3s.io/) for further details.

## Installation using YAOOK Kubernetes

For production setups, we currently recommend the manual use of YAOOK Kubernetes which is also used for deploying the YAOOK cluster itself. This gives you tested and automated upgrades to new Kubernetes versions.

To learn about YAOOK Kubernetes in general and its concepts for manual use in depth, please see the upstream documentation (TODO).

**Note:** Throughout this guide, you will see references to `masters`. This is the name of the group of hosts which are used for the Kubernetes control plane. Unfortunately, the `master` terminology is baked pretty deeply into Kubernetes and we took that over. The opaque (technical) strings in Kubernetes cannot be changed easily. There is a [Naming Working Group](https://github.com/kubernetes/community/tree/master/wg-naming) within the Kubernetes project to address this, with specific [recommendations to replace `master` with `control-plane`](https://github.com/kubernetes/community/blob/master/wg-naming/recommendations/001-master-control-plane.md). We are not quite there yet, though.

### Preparing the Cluster Repository

The cluster repository holds all cluster-specific information. Commonly, it is a git repository, even though it is not required that it has any remotes. It is recommended to back this repository up to a secure location as it will contain sensitive and important material for further operations on the Management Kubernetes cluster.

We are going to assume that the cluster repository is in `~/mgmt-cluster`. If it is not, please adapt the paths in the remaining section accordingly.

```bash
git init ~/mgmt-cluster
cd ~/mgmt-cluster
git submodule add https://gitlab.com/yaook/k8s.git/ managed-k8s
mkdir -p config
cp managed-k8s/templates/template.gitignore .gitignore
cp --no-clobber managed-k8s/templates/config.template.toml config/config.toml
git add managed-k8s config/config.toml .gitignore
git commit -m 'Bootstrap cluster repository using the Installation Guide'
```

### Installing Ansible

YAOOK Kubernetes uses Ansible in order to configure the nodes. Currently, it needs Ansible 2.9 or newer. In order to ensure that you have the correct version of ansible installed, it is recommended to use a Python virtual environment.

```bash
cd ~/mgmt-cluster
virtualenv venv
. ~/mgmt-cluster/venv/bin/activate
```

Make sure to always execute `~/mgmt-cluster/venv/bin/activate` before working with the cluster, or use a tool like [direnv](https://direnv.net/) to manage the virtual environment variables for you.

```bash
pip3 install -r managed-k8s/requirements.txt
```

### Configuring your Environment

To install the management cluster, you need to export the following environment variables:

```bash
[terraform]
enabled = false

[wireguard]
enabled = false
```

The first disables the terraform-based provisioning of nodes. The second disables the Wireguard VPN gateway.

If you do not disable wireguard, you end up with errors during the `update_inventory.py` step later on.

### Writing the Inventory File

YAOOK Kubernetes uses Ansible in order to configure the nodes. Ansible uses inventory files to know which nodes are assigned which tasks and how to connect to the nodes.

The inventory files need to be placed in specific locations and adhere to a specific structure of host groups in order for YAOOK Kubernetes to work. Assuming you are in your cluster repository, you bootstrap your inventory like this:

```bash
cd ~/mgmt-cluster
mkdir -p inventory/{02_trampoline,03_final,03_k8s_base}
touch inventory/02_trampoline/hosts
ln -sf ../02_trampoline/hosts inventory/03_final/hosts
ln -sf ../02_trampoline/hosts inventory/03_k8s_base/hosts
```

`02_trampoline` and `03_final` refer to two different stages of a YAOOK Kubernetes deployment. As we are operating in non-cloud, bare-metal standalone mode, the two stages use exactly the same inventory files.

The inventory file syntax is [explained in full detail in the Ansible manual](https://docs.ansible.com/ansible/2.9/user_guide/intro_inventory.html). Please refer to that if the example here does not fully cover your use case.

In this example, we are deploying a single-node cluster to a host called `iro-tura-5016` with the IPv4 `10.2.16.0`. It has been deployed with a Ubuntu 22.04 LTS image so that we can log in as `ubuntu` user and can gain root via passwordless sudo (for ansible to work nicely).

(Note: The name `iro-tura-5016` has been randomly generated and is not magic in any way.)

The cluster will have a load-balancing IPv4 `10.4.1.5` which will be provided via HAProxy / keepalived. This will be used for accessing the Kubernetes API and can also be used to access exposed NodePort services. We will make use of that later to expose the nginx ingress.

If you need dualstack on the Kubernetes layer, you may try to set `dualstack_support=True` in the inventory as well as setting a `networking_fixed_ip_v6` for load-balanced IPv6 API access. This has not been tested in a while though. You do not need IPv6 on the Kubernetes layer to have IPv6 networks in OpenStack.

Using your favourite editor, open `inventory/02_trampoline/hosts` and fill in an inventory. The following is an example inventory with a single-node Management Kubernetes cluster:

```ini
[all:vars]
dualstack_support=False
networking_fixed_ip=10.4.1.5
on_openstack=False

[k8s_nodes:children]
masters
workers

[frontend:children]
masters

[masters]
iro-tura-5016 ansible_host=10.2.16.0 ansible_user=ubuntu local_ipv4_address=10.2.16.0

[workers]
```

To add more nodes, add them to either the `masters:` section (where `iro-tura-5016` is placed already) or to the `workers:` section (if they should not run k8s control plane pods).

Validate your inventory file by running:

```bash
ansible -i inventory/02_trampoline/hosts all -m command --args hostname
```

It should print the hostname of each of your machines:

```console
$ ansible -i inventory/02_trampoline/hosts all -m command --args 'hostname'
iro-tura-5016 | CHANGED | rc=0 >>
iro-tura-5016
```

**Note:** We used to recommend CentOS. We have since switched the stack to Ubuntu-only. You may find remnants fo the CentOS era in some bits, but we suggest you mentally translate those to Ubuntu specifics.

### Configuring the Cluster

The main configuration entrypoint is the `config/config.toml` file. It is initialized with an example configuration, though it should be noted that it is geared toward a non-bare-metal cluster due to the origins of YAOOK Kubernetes.

The `config.toml` has extensive inline documentation. This handbook will provide you with an example, but it is strongly recommended that you read the reference inside the example `config.toml` to see which knobs you are setting.
You most probably have to adapt the `subnet_cidr` in order to match the network of your nodes. For obvious reasons we don't have a Terraform section in bare metal setups which typically provides that variable.

```toml
[load-balancing]
openstack_lbaas = false

[ch-k8s-lbaas]
enabled = false

[kubernetes]
version = "1.23.4"
use_podsecuritypolicies = false
is_gpu_cluster = false

[kubernetes.storage]
rook_enabled = false
nodeplugin_toleration = true

[kubernetes.local_storage.static]
enabled = false

[kubernetes.local_storage.dynamic]
enabled = true

[kubernetes.monitoring]
enabled = false

[kubernetes.global_monitoring]
enabled = false

[kubernetes.network]
pod_subnet = "10.244.0.0/16"
service_subnet = "10.96.0.0/12"
plugin = "calico"

[k8s-service-layer.prometheus]

[k8s-service-layer.cert-manager]
enabled = true

[k8s-service-layer.ingress]
enabled = true

[node-scheduling]
scheduling_key_prefix = "scheduling.mk8s.cloudandheat.com"

[node-scheduling.labels]

[node-scheduling.taints]
# When spawning a single node cluster or a cluster without dedicated workers,
# you have two options to allow workload to be scheduled on the control-plane.
# Either set the line beneath for each node, or after creating the cluster run:
# kubectl taint nodes NODE_NAME role.kubernetes.io/master:NoSchedule-
#
iro-tura-5016 = []

[testing.test-nodes]

[wireguard]
enabled = false
rollout_company_users = false

[ipsec]
enabled = false # •ᴗ•

[passwordstore]
rollout_company_users = false

[[passwordstore.additional_users]]
ident = "john.doe@company.example"
gpg_id = "0123456789ABCDEF2342DEADBEEF2342CAFE2342"

[cah-users]
rollout = false  # •ᴗ•

[miscellaneous]
subnet_cidr = "172.30.154.0/24"
wireguard_on_workers = false
```

Once you have written the `config.toml`, it needs to be compiled into ansible variables using the `update_inventory.py` script:

```console
$ python3 managed-k8s/actions/update_inventory.py
```

This command should give no errors. It will create files and directories under `inventory` where the compiled configuration is stored.

### Deploying the Cluster

YAOOK Kubernetes comes with ready-made scripts to deploy a cluster given that you have the inventory set up already. All you need to do is to invoke them in the correct order:

```console
$ ./managed-k8s/actions/apply.sh
```

**Note:** Depending on your exact SSH setup, it is possible that the test stage
fails on the first attempt. You can re-run the tests using `./managed-k8s/actions/test.sh`, at which point they should pass without any error.
