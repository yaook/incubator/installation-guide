# Inventorizing Nodes

The source of truth for nodes is the NetBox. The OpenStack Ironic inspection feature is used to detect which nodes are in the field and what their state is. This is synchronized into the NetBox by the metal controller.

By default, nodes are recognized by their BMC/IPMI MAC address. The matching mode can be changed using the `NETBOX_NODE_MATCH_*` environment variables (use the source, Luke!).

## Preparing the node in NetBox

1. Create a Device Role for the node if you do not have one already. The three custom fields need to be filled in for the role to work with the metal controller. **Important:** If the custom fields are *not* filled in, the metal controller will ignore the node, even if it is matched by a node in Ironic.

    - `yaook_hostname_prefix` must be a three-character prefix which will be used if a hostname is generated automatically for the node
    - `yaook_scheduling_keys` is currently unused and may be left blank.
    - `yaook_control_plane` must be set to true in order to use nodes with this role as kubernetes control plane nodes and false otherwise.

2. Create the device and assign a valid device role (see above). Set the node status to `Planned` to avoid it getting deployed right away. You may leave the device name blank to trigger auto-generation of a hostname. Otherwise, the device name must be a valid hostname.

3. Ensure that the device has an interface with a matching MAC address (or whichever node matching you configured).

4. Unless you changed `NETBOX_IPMI_VAULT_PATH_TEMPLATE`, assign an asset tag to the node.

5. Configure the interfaces of the device. The metal controller will attempt to match them based on their MAC address, or if that fails, based on the name. Before proceeding, you should assign VLANs to the interfaces (tagged or untagged or a mixture), as well as optionally (if you're not using IP address auto-allocation

6. Add the node to the corresponding Yaook cluster. Navigate to virtualization -> clusters, select the cluster and hit Assign Device. Select the newly created device to add it to the cluster.

## Preparing the IPMI credentials

Unless you changed `NETBOX_IPMI_VAULT_PATH_TEMPLATE`, you need to store the IPMI user name and password in the vault like this:

```console
$ vault kv put yaook/<CLUSTER-FQDN>/kv/ipmi/<ASSET-TAG> username=<IPMI-USERNAME> password=<IMPI-PASSWORD>
```

Replace:

- `<CLUSTER-FQDN>` with the FQDN of the Cluster object in NetBox (the custom field value)
- `<ASSET-TAG>` with the Asset tag of the device in NetBox
- `<IPMI-USERNAME>` with the IPMI username for the device. Make sure to quote any special characters accordingly.
- `<IPMI-PASSWORD>` with the IPMI password for the device. Make sure to quote any special characters accordingly.

## Discovering the node using Ironic

Integrate the physical device into the cluster by connecting it to the network as required. Make sure that it is connected to the same network as the Ironic dnsmasq/PXE service.

Power on the node and ensure that it is booting from the network. It should be loading the Ironic Python Agent. After a short while (2-3 minutes after booting into the IPA), the node should appear in `openstack baremetal node list` from within the `infra-ironic-client-*` pod in your management cluster.

If this is not the case, you'll have to troubleshoot the PXE boot. Sorry.

## Observe synchronization toward NetBox

Once the node is discovered by Ironic, you should see that the metal-controller is synchronizing some information into NetBox. Specifically, it should add a comment to the node which shows the BMC IPMI address.

If this is not the case, run `openstack baremetal node show` on the node; an error message should be part of the extra fields. If this is not the case either, verify that the metal-controller can talk to both Ironic and the NetBox and that it is not printing any errors into its log.

The metal-controller will attempt to enroll the node into Ironic, as well as triggering cleaning. Any errors encountered during that will be written into the comment field of the Device in NetBox.
