# Glance

The resource to deploy glance is a GlanceDeployment. The skeleton of the resource looks like this:

```yaml
apiVersion: yaook.cloud/v1
kind: GlanceDeployment
metadata:
    name: glance
    namespace: yaook
spec:
    ..
```

The full reference for fields available on the `GlanceDeployment` is available
in the
[API reference documentation](https://docs.yaook.cloud/handbook/api-reference.html#user-api-reference-glancedeployment).

The most important keys you'll have to think about are:

* `spec.targetRelease` to set the OpenStack release to deploy.
* `spec.keystoneRef.name` needs to be set to the name of your KeystoneDeployment.
* `spec.api.ingress.fqdn` and `spec.api.ingress.port`, to make the external Glance endpoints reachable.
* `spec.database.storageClassName` to ensure the database volumes use the correct storage class.
* `spec.database.storageSize` to ensure the database volumes has sufficient space.
* `spec.issuerRef.name` to make YAOOK able to issue certificates for internal use; should be set to `yaook-internal` if you have been following this guide
* `spec.region.name` to consistently set the region throughout your OpenStack deployment.
* `spec.backends`: Configure at least one backend for your image store; see the reference documentation for details.

    When using `ceph` and Rook within the same cluster, you can apply

    ```
    apiVersion: ceph.rook.io/v1
    kind: CephClient
    metadata:
        name: glance
    spec:
        caps:
            mon: 'profile rbd'
            osd: 'profile rbd pool=<pool name>'
    ```

    in the Ceph namespace (make sure to replace `<pool name>` with the name of
    the image pool to use). This will generate a secret called
    `rook-ceph-client-glance` which you need to copy over to the `yaook`
    namespace and reference in the `spec.backends.ceph.keyringReference` field.

Once you have built your GlanceDeployment resource (you may use the [official GlanceDeployment example as reference](https://docs.yaook.cloud/examples/glance.html), though be careful that its values are not safe for production!), you can apply it with kubectl into the yaook namespace.

After that, you can watch the progress by calling `yaookctl status openstack`.

Once the Glance entry in `yaookctl status openstack` has reached `Success` state, you can use `yaookctl shell openstack` to get a shell which has OpenStack credentials loaded. In that shell, you can run `openstack image list` to verify that Glance is working (it should produce empty output).
