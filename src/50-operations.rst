Operations
==========

In the following we describe specific operation procedures to be used for YAOOK.
Each of them is written in the form of a manual that you can follow though.


.. toctree::
   :maxdepth: 1

   50-00-database-operations.md
