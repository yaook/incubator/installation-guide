:_creating_a_cluster:

Creating a Cluster
==================

Two key ingredients are needed to create a cluster. First, a Cluster object
needs to be created in the Netbox. Second, the cluster repository needs to be
initialized in the metal controller's cluster repository volume.

Creating the Cluster in NetBox
------------------------------

Creating the Cluster object
^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. If you do not have a Cluster Type you want to use already, click on Virtualization -> Cluster Types and then Add. The Slug of the Cluster Type must be `yaook`, unless you changed the `NETBOX_CLUSTER_TYPES` environment variable of the metal controller.

2. In Virtualization -> Clusters, click Add.

3. Fill in the details of the Cluster. The metal controller, by default, only cares about the `yaook_domain` custom field: This is the unique identifier for the cluster. Set it to an FQDN uniquely identifying this Yaook cluster.

Mapping the network space
^^^^^^^^^^^^^^^^^^^^^^^^^

1. Create at least one VRF for your cluster.

2. Create VLANs and the topmost prefixes **within that VRF** for your cluster.

3. Create IP addresses for the default gateway and any nameservers in the corresponding prefixes, using the tags you created earlier to mark them.

4. If you want to make use of the automatic IP address allocation feature, you may want to create sub-prefixes within the topmost prefix (which will be used to determine the netmask)

5. Reserve an IPv4 address for the Kubernetes API to use. This IP address will be assigned via VRRP to any of the control plane nodes.

Creating the Cluster Repository
-------------------------------

1. Open a shell in the `metal-controller-0` pod.

2. Run `/docker-run/mkcluster /var/lib/metal-controller/<FQDN>/`, where `<FQDN>` is to be substituted with the FQDN you used in the NetBox above.

    This clones the lifecycle management repository and initializes the cluster with a default configuration.

3. Open `/var/lib/metal-controller/<FQDN>/config.toml` and apply the following workarounds/hacks:

    1. In the `[misc]` section, add the following variables:

        - `networking_fixed_ip`, set to the IP address you reserved for the k8s API.
        - `networking_floating_ip`, set to the same IP address
        - `networking_fixed_ip_v6`, set to e.g. `fd00::ffff:ffff:ffff:ffff`. This address is unused because dual stack support is currently disabled, but due to a bug the field is read nonetheless.

    2. In the `[ch-k8s-lbaas]` section, set `enabled` to false.

    3. Uncomment the line `[[passwordstore.additional_users]]` and the two following lines. The values do not matter because no data is actually written into the password store in a bare metal setup. However, at least one ID is required for the initialization of the password store to complete.

    4. Ensure that pod security policies are disabled and that the calico networking plugin is selected.

    5. Enable the dynamic local storage provisioner and disable the static local storage provisioner.

        **NOTE:** This needs https://gitlab.com/yaook/k8s/-/merge_requests/383, so if that is not merged, make sure to check out your `managed-k8s` submodule to that branch and `git add` the `managed-k8s` submodule.

4. Run `git add config/config.toml` and commit the state. Otherwise, the metal controller will discard the changes before rolling out.

Creating the Cluster credential store in Vault
----------------------------------------------

With a sufficiently privileged Vault token, run [`vault-mkcluster.sh` as found in the metal-controller repository](https://gitlab.com/yaook/metal-controller/-/blob/devel/utils/vault-mkcluster.sh) with the cluster FQDN as only argument.

This will create the key-value store as well as the SSH CA for the cluster. The public key of the SSH CA will be printed to stdout, for further use in e.g. an `known_hosts` file.

.. toctree::
   :maxdepth: 1
   :caption: Next steps

   20-10-adding-nodes.md
   20-20-deploying.md
