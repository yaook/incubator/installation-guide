# Glance

The resource to deploy cinder is a CinderDeployment. The skeleton of the resource looks like this:

```yaml
apiVersion: yaook.cloud/v1
kind: CinderDeployment
metadata:
    name: cinder
    namespace: yaook
spec:
    ..
```

The full reference for fields available on the `CinderDeployment` is available
in the
[API reference documentation](https://docs.yaook.cloud/handbook/api-reference.html#user-api-reference-cinderdeployment).

The most important keys you'll have to think about are:

* `spec.targetRelease` to set the OpenStack release to deploy.
* `spec.keystoneRef.name` needs to be set to the name of your KeystoneDeployment.
* `spec.api.ingress.fqdn` and `spec.api.ingress.port`, to make the external Cinder endpoints reachable.
* `spec.database.storageClassName` to ensure the database volumes use the correct storage class.
* `spec.database.storageSize` to ensure the database volumes has sufficient space.
* `spec.messageQueue.storageClassName` to ensure the rabbitmq volumes use the correct storage class.
* `spec.issuerRef.name` to make YAOOK able to issue certificates for internal use; should be set to `yaook-internal` if you have been following this guide
* `spec.region.name` to consistently set the region throughout your OpenStack deployment.
* `spec.backends`: A map of backends. The left-hand side of the map is the backend name, the right-hand side configures the backend. See the reference documentation for more details.

When using `ceph` and Rook within the same cluster, you can apply

```
apiVersion: ceph.rook.io/v1
kind: CephClient
metadata:
    name: cinder
spec:
    caps:
        mon: 'profile rbd'
        osd: 'profile rbd pool=<pool name>, profile rbd-read-only pool=<glance pool name>'
```

in the Ceph namespace (make sure to replace `<pool name>` with the name of the
volume pool to use). This will generate a secret called
`rook-ceph-client-cinder` which you need to copy over to the `yaook` namespace
and reference in the `spec.backends.rbd.keyringReference` field.

Once you have built your CinderDeployment resource (you may use the [official CinderDeployment example as reference](https://docs.yaook.cloud/examples/cinder.html), though be careful that its values are not safe for production!), you can apply it with kubectl into the yaook namespace.

After that, you can watch the progress by calling `yaookctl status openstack`.

Once the Cinder entry in `yaookctl status openstack` has reached `Success` state, you can use `yaookctl shell openstack` to get a shell which has OpenStack credentials loaded. In that shell, you can run:

1. `openstack volume create --size 10 testvolume`
2. `watch openstack volume show testvolume`

It should become available after a few seconds.
