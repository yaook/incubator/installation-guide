Installation guide
==================

Introduction
============

About YAOOK
-----------

YAOOK is a Life Cycle Management for OpenStack and SecuStack clusters. It is separated in three layers:

- YAOOK Bare Metal: Management of the hardware inventory which is used to form the cluster. This includes inventorisation and installation of operating systems, as well as configuration of storage devices.

- YAOOK Kubernetes: Management of the Kubernetes cluster running on the hardware. This includes node lifecycling, management of some supporting services (such as a Rook-based storage cluster) and Kubernetes upgrades.

- YAOOK Operator: Management of OpenStack services inside the Kubernetes cluster.

Deciding whether to use the Bare Metal layer
--------------------------------------------

The heart of the YAOOK Bare Metal layer is the Metal Controller. That piece of software integrates with Ironic Bare Metal and other tools in order to allow your cloud to scale quickly: Instead of installing an operating system and running yaook/k8s manually whenever you integrate a new node, the Bare Metal layer automates this by deploying an OS and yaook/k8s in a single automated step, which can be parallelized for many nodes.

However, if you intend to set up only a proof-of-concept focusing on the YAOOK Operator layer or if you do not intend to ever scale beyond a handful of nodes, you can decide to omit this layer.

In that case, you can use yaook/k8s to install Kubernetes directly. You may roughly follow the Management Cluster installation guide of this book in order to learn how to deploy yaook/k8s on bare metal and then skip right to the Installing OpenStack chapter to deploy YAOOK Operator on top of that.

The concrete trade-off is scalability vs. initial cost: Installing the metal controller takes time and resources, but it allows you to scale your cluster much faster down the road.

It is *possible*, albeit undocumented and possibly difficult, to migrate an existing cluster into a metal controller.

Installation Process and Components
-----------------------------------

The installation process consists of four major steps:

1. Installation of the hardware inventory management tools. Those tools are:

    - NetBox to keep track of the inventory
    - HashiCorp Vault to manage secrets
    - OpenStack Ironic (in standalone mode) to install the cluster nodes
    - YAOOK Metal Controller which orchestrates the node installation

2. Forming of a Kubernetes cluster. Using the Kubernetes Lifecycle Management, one or more clusters are built on top of the hardware nodes.

3. Installation of OpenStack using the YAOOK Operator.

Table of Content
----------------

.. toctree::
   :maxdepth: 1

   05-00-prerequisites.md

.. toctree::
   :maxdepth: 2
   :caption: Installing the Management Cluster

   10-00-management-cluster.md

.. toctree::
   :maxdepth: 2
   :titlesonly:
   :caption: Creating a Cluster

   20-00-creating-a-cluster.md

.. toctree::
   :maxdepth: 2
   :caption: Installing OpenStack

   30-installing-openstack.md

.. toctree::
   :maxdepth: 2
   :caption: Migrating an existing OpenStack Cluster

   40-00-migrating-openstack.md

.. toctree::
   :maxdepth: 3
   :caption: Operations

   50-operations.md
