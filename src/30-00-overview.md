# OpenStack Overview

## Motivation

In order to install a working YAOOK cluster, it is important to have a solid understanding of the OpenStack components involved. If you are already familiar with OpenStack itself, you may safely skip this entire page and move on to the next.

## Component Overview

OpenStack consists of many microservices. Of these, the following are RECOMMENDED to be present in any YAOOK cluster:

- [Keystone](https://docs.openstack.org/keystone/latest/), the authentication (users, projects, groups) service.
- [Glance](https://docs.openstack.org/glance/latest/), the image (think disk image) service.
- [Cinder](https://docs.openstack.org/cinder/latest/), the block storage (think volumes) service.
- [Neutron](https://docs.openstack.org/neutron/latest/), the networking (virtual networks, routers and layer 2 ports) service.
- [Nova](https://docs.openstack.org/nova/latest/), the compute (virtual machines) service.

Except for Keystone and Glance, all of these services have multiple components which deserve further explanation. These will be explained in the next sections.

### Cinder Components

- cinder-api - API server, also writes to the database
- cinder-volume - Interacts with the configured backend(s) to do the actual
  volume options, like create, delete, resize (if volume is not attached,
  based on backend driver), snapshot creation
- cinder-backup - Based on configured backend driver it creates backups of the
  volumes (and may some other resources)

### Neutron Components

- neutron-server - API server, writes to the database, communicates via
  configured ml2 plugin with network backend (ovn-setup: northbound-ovsdb)
- OVN backend/ovsdb - not really part of neutron itself, but neutron writes all
  information to it, so OVN can create the network setup defined at neutron

### Nova Components

- nova-api - API server
- nova-scheduler - selects based on filters and other where to run a VM
- nova-conductor - mainly keeps track of state of nova-compute nodes
- nova-compute - service on each hypervisor where VMs should run. Interacts
  with some tools to create and maintain the VMs.
