# Keystone

The resource to deploy keystone is a KeystoneDeployment. The skeleton of the resource looks like this:

```yaml
apiVersion: yaook.cloud/v1
kind: KeystoneDeployment
metadata:
    name: keystone
    namespace: yaook
spec:
    ..
```

> **Note:** Even though you are free to choose any name for your KeystoneDeployment, using `keystone` has the advantage that a few things assume that by default and you'll have to do less overriding.

The full reference for fields available on the `KeystoneDeployment` is
available in the
[API reference documentation](https://docs.yaook.cloud/handbook/api-reference.html#user-api-reference-keystonedeployment).

The most important keys you'll have to think about are:

* `spec.targetRelease` to set the OpenStack release to deploy.
* `spec.api.ingress.fqdn` and `spec.api.ingress.port`, to make the external Keystone endpoints reachable.
* `spec.database.storageClassName` to ensure the database volumes use the correct storage class.
* `spec.database.storageSize` to ensure the database volumes has sufficient space.
* `spec.issuerRef.name` to make YAOOK able to issue certificates for internal use; should be set to `yaook-internal` if you have been following this guide
* `spec.region.name` to consistently set the region throughout your OpenStack deployment.

Once you have built your KeystoneDeployment resource (you may use the [official KeystoneDeployment example as reference](https://docs.yaook.cloud/examples/keystone.html), though be careful that its values are not safe for production!), you can apply it with kubectl into the yaook namespace.

After that, you can watch the progress by calling `yaookctl status openstack`.

Once the rollout has finished (the `Keystone` entry in `yaookctl status openstack` reaching `Success` status), you can use `yaookctl shell openstack` and run `openstack catalog list` in there to ensure that Keystone has been deployed correctly.
