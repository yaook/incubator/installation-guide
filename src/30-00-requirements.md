# YAOOK Operator - Requirements and Preparation

## Introduction

The following pages will guide you through the setup of Yaook operator on top
of a kubernetes cluster. You can use
[yaook-k8s](https://yaook.gitlab.io/k8s/devel/introduction.html) therefore
but also every other k8s installation works. (For more detail see Requirements
section.)

## Requirements

Please ensure that the following general requirements are met:

- [OS requirements](https://docs.yaook.cloud/requirements/os.html#requirements-os)
- [Kubernetes API requirements](https://docs.yaook.cloud/requirements/k8s-api.html#requirements-k8s-api)
- [Kubernetes cluster requirements](https://docs.yaook.cloud/requirements/k8s-cluster.html#requirements-k8s-cluster)

Note:

    Although we recommend using yaook/k8s to do so, we provide scripts for
    installing prometheus, cert-manager, nginx ingress controller and
    rook/ceph. This means that even if some required features are not yet
    present in your kubernetes cluster you can go ahead and follow these
    instructions.

## Requirements for the Operator Setup

Please ensure that the following requirements are met, when setting up a
yaook deployment:

- You have access to a Kubernetes cluster

    - either via the default ``~/.kube/config``, or alternatively
    - using the ``KUBECONFIG`` environment variable.

- The [kubectl](https://kubernetes.io/docs/reference/kubectl/overview/) and
  [helm](https://helm.sh/docs/intro/install/) binaries are in your path.

## Preparation of Environment and Kubernetes Resources

1. Set the Environment Variables, also see 
    [Environment Variable Reference](https://docs.yaook.cloud/handbook/env-reference.html#uref-operator-env-vars):

    ```bash
    # Used to determine which namespaces are relevant for the operator
    export YAOOK_OP_NAMESPACE="yaook"
    export NAMESPACE=$YAOOK_OP_NAMESPACE
    ```

1. Create the k8s namespace and deploy helm central charts:

    ```bash
    kubectl get namespace $NAMESPACE 2>/dev/null || kubectl create namespace $NAMESPACE
    # Deploy helm charts for central services
    helm repo add stable https://charts.helm.sh/stable
    helm repo update
    ```

1. **Optional:** Depending on which features you still need to deploy in your
    kubernetes cluster, execute the following scripts:	

    ```bash
    curl -sSL https://gitlab.com/yaook/operator/-/raw/devel/docs/getting_started/install_prometheus.sh | bash
    curl -sSL https://gitlab.com/yaook/operator/-/raw/devel/docs/getting_started/install_ingress_controller.sh | bash
    curl -sSL https://gitlab.com/yaook/operator/-/raw/devel/docs/getting_started/install_cert_manager.sh | bash
    ```

    See the
    [kubernetes cluster requirements](https://docs.yaook.cloud/requirements/k8s-cluster.html#requirements-k8s-cluster)
    for more information. Adjust versions and variables to your needs.

1. Either
    - if you want to use ceph as storage (recommended):
    
        - check e.g. https://yaook.gitlab.io/k8s/devel/user/reference/cluster-configuration.html#rook-configuration
          on how to install ceph-rook at yaook-k8s.

    - or configure another storage backend at ``spec:backends`` in
      ``{nova,cinder,glance}.yaml`` and
      ``spec:glanceConfig:glance_store:default_store`` to ``{YOUR_BACKEND}``
      in ``glance.yaml``. Also check the following pages about Glance, Cinder
      and Nova.
