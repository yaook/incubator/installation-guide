# Preparations

## Label the nodes

Before deploying the operators and moving further, it is important that you have labelled your nodes correctly according to the label & taint discussion from earlier. Otherwise, the operators may not come up.

For example you could label the control plane nodes like this:
```bash
#!/bin/bash
ctl_nodes="control-plane-node-1 control-plane-node-2 control-plane-node-3" 

ctl_plane_labels="node-role.kubernetes.io/control-plane=true 
 any.yaook.cloud/api=true 
 infra.yaook.cloud/any=true 
 operator.yaook.cloud/any=true 
 key-manager.yaook.cloud/barbican-any-service=true 
 block-storage.yaook.cloud/cinder-any-service=true 
 compute.yaook.cloud/nova-any-service=true 
 ceilometer.yaook.cloud/ceilometer-any-service=true 
 key-manager.yaook.cloud/barbican-keystone-listener=true 
 gnocchi.yaook.cloud/metricd=true 
 infra.yaook.cloud/caching=true 
 network.yaook.cloud/neutron-northd=true"

for node in $ctl_nodes; do
    kubectl label node "$node" $ctl_plane_labels
done
```

And similarly for the worker nodes:
```bash
#!/bin/bash
hypervisor_nodes="worker-node-1 worker-node-2"

for node in $hypervisor_nodes; do
    kubectl label node "$node" "compute.yaook.cloud/hypervisor"
done
```
## Install yaookctl

`yaookctl` is a command line tool which makes interactions with YAOOK-based OpenStack clusters much more pleasant. Most of its value is in its ability to resolve indirections, e.g. finding the correct pod of a service's SQL database.

There are two ways to use `yaookctl`:

- Local installation via pip. This is recommended.

    1. Clone the repository from https://gitlab.com/yaook/yaookctl/ to a location on your computer.
    2. Run `pip install -e /path/to/the/clone/of/yaookctl` in order to install yaookctl locally.

- Installation into a Kubernetes cluster:

    > **Warning**: This creates an, within the yaook namespace, almighty ServiceAccount. The ServiceAccount can be used by *anyone* with the privilege to create Pods (or things which create Pods, such as Deployments, StatefulSets, Jobs, CronJobs or DaemonSets) in the `yaook` namespace to escalate their own privileges to admin level.

    ```
    $ curl -sSL https://gitlab.com/yaook/yaookctl/-/blob/devel/examples/yaookctl.yaml | kubectl -n yaook apply -f -
    ```

    Once installed, yaookctl can be used from `kubectl exec` like this:

    ```
    $ kubectl -n yaook exec -it deploy/yaookctl -- /bin/bash
    root@yaookctl-8b76b9cb8-r7h88:/# yaookctl --help
    Usage: yaookctl [OPTIONS] COMMAND [ARGS]...
    ```

For more information on how to use yaookctl, consult the built-in `--help`, or [the yaookctl README](https://gitlab.com/yaook/yaookctl/-/blob/devel/README.md).

Confirm that yaookctl is installed correctly by running `yaookctl status openstack`. If you are running this on a fresh Kubernetes cluster, you should be seeing a table like this:

```
+------------------------+----------+--------+-------+---------+-----------+
| Kind                   | Name     | Status | Since | Message | Workloads |
+------------------------+----------+--------+-------+---------+-----------+
| Keystone               | <absent> |        |       |         |           |
[…]
```

with all "\<absent\>" entries.

## Add the YAOOK helm repository

1. Install [helm](https://helm.sh/) if you haven't already.

2. Add the official YAOOK helm repository:

    ```
    $ helm repo add yaook.cloud https://charts.yaook.cloud/operator/stable/
    ```

3. Verify that it works by seeing if you can find the `crds` helm chart:

    ```bash
    $ helm search repo yaook.cloud/crds
    NAME            	CHART VERSION	APP VERSION 	DESCRIPTION
    […]
    yaook.cloud/crds	0.20240704.1 	0.20240704.1	CustomResourceDefinitions used by Yaook.
    […]
    ```

## Pick a YAOOK release to run

At the time of writing, YAOOK features a fortnightly automated release cycle. That means that every other week, a new stable version is released. Old stable versions are not supported.

The best choice is generally to use the most recent stable and be prepared to upgrade regularly. The most recent release can be obtained from helm:

```bash
$ helm search repo yaook.cloud/crds
NAME            	CHART VERSION	APP VERSION 	DESCRIPTION
yaook.cloud/crds	0.20240704.1 	0.20240704.1	CustomResourceDefinitions used by Yaook.
```

There, you can see the version `0.20240704.1` as being the most recent version.

Export the version for tools and commands to use:

```bash
$ export YAOOK_VERSION=0.20240704.1
```

## Pick a namespace to install in

It is recommended that you use the `yaook` namespace. While yaook supports running in arbitrarily named namespaces, some guides and documentation will assume the namespace name `yaook`.

Export the namespace name for tools to use:

```
$ export YAOOK_OP_NAMESPACE=yaook
```

## Pick a domain name

An OpenStack deployment will need several domain names, one for each OpenStack service. It is recommended that you pick a domain name and put all services below that. For instance, if you chose `yaook.example`, you would have subdomains like `identity.yaook.example` etc.

## Pick an OpenStack release

Just use Zed for now.

## Install the CRDs

> **Note:** This section assumes that you exported `YAOOK_VERSION` and `YAOOK_OP_NAMESPACE` as described above in your current shell.

The `crds` helm chart contains all [Custom Resource Definitions](https://kubernetes.io/docs/tasks/extend-kubernetes/custom-resources/custom-resource-definitions/) maintained by YAOOK. It must be installed before any other YAOOK service is installed and, when upgrading, it must be the first chart you upgrade.

```
$ helm upgrade --install -n "$YAOOK_OP_NAMESPACE" --version "$YAOOK_VERSION" crds yaook.cloud/crds
```

## Install the infrastructure service operator

> **Note:** This section assumes that you exported `YAOOK_VERSION` and `YAOOK_OP_NAMESPACE` as described above in your current shell.

Databases, message queues and caches are managed by the `infra-operator`. It must be deployed before any OpenStack service is configured, as nearly all OpenStack services need at least one of these.

```
$ helm upgrade --install -n "$YAOOK_OP_NAMESPACE" --version "$YAOOK_VERSION" infra-operator yaook.cloud/infra-operator
```

> **Warning:** The infra operator is **the most critical operator of all of
    YAOOK**. It **must** be kept running: It is responsible for exchanging
    certificates on database and message queues in time. If it is not running
    or the hole cluster is turned off for more than a few days, this may cause
    your cluster to break.

## Install the OpenStack service operators

> **Note:** This section assumes that you exported `YAOOK_VERSION` and `YAOOK_OP_NAMESPACE` as described above in your current shell.

Each operator comes with its own helm chart and can be installed separately. This allows you to choose exactly the operators you want for your cloud. Each OpenStack service is represented by at least one YAOOK operator, though some use more than one.

For those services using more than one operator, the following table lists which operators you need to deploy the service successfully:

| OpenStack service | Operators |
| --- | --- |
| Keystone | `keystone`, `keystone-resources` |
| Nova | `nova`, `nova-compute` |
| Placement | `nova` |
| Neutron (OVN) | `neutron`, `neutron-ovn`, `neutron-ovn-bgp`(if needed) |

The other operators are called identically to their respective OpenStack project.

**Note:** Placement, even though a separate upstream service these days, is fully integrated in the YAOOK nova operator.

The easiest way to install a group of operators is using a shell loop like this example for keystone:

```
$ for op in keystone keystone-resources; do helm upgrade --install -n "$YAOOK_OP_NAMESPACE" --version "$YAOOK_VERSION" "$op-operator" "yaook.cloud/$op-operator"; done
```

## Create an internal Certificate Authority

> **Note:** This section assumes that you exported `YAOOK_OP_NAMESPACE` as described above in your current shell.

YAOOK uses TLS for all internal communication. In order to be able to dynamically provision services, an internal certificate authority under control of cert-manager is used.

To create this CA, deploy the following YAMLs:

```yaml
---
apiVersion: cert-manager.io/v1
kind: Issuer
metadata:
  name: selfsigned-issuer
spec:
  selfSigned: {}
---
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: yaook-internal-ca
spec:
    isCA: true
    commonName: yaook-internal-ca
    secretName: yaook-internal-ca
    issuerRef:
        name: selfsigned-issuer
---
apiVersion: cert-manager.io/v1
kind: Issuer
metadata:
  name: yaook-internal
spec:
  ca:
    secretName: yaook-internal-ca
```
