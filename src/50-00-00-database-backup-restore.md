# Backup Restore

All Yaook database clusters automatically do regular backups.
Depending on your configuration these backups are either only saved in the ephemeral storage of the backup container or copied somewhere else.

## Preparing the backup

As the first part of the restore procedure determine the backup you want to use for a restore.
What exact backup you need can be determined by checking the timestamp of the backup against the time of the error you want to recover from.

After you have determined the backup location you need copy it to some machine to prepare it for restoration.
If you store your backup in s3 you can for example use `s3cmd` to retrieve the file.
If you store your backup only in the ephemeral storage of the backup container you can use e.g. `kubectl cp -c backup-shifter <pod-name-of-db>:/backup/warm/<backup-file-name> backup.tgz`.

Unpack the downloaded `tgz` file.

## Determining the restore type

For the restore you have two options available:

<dl>
<dt>Restore of Mariabackup</dt>
<dd>This restores a consistent backup of the whole database.
    It requires the database to be offline for the time of the restore.
    If you need to restore the complete db this is the way to go.</dd>

<dt>Restore of mysqldump</dt>
<dd>This restores a non-consistent backup of each table of the database.
    The backup can also be edited to only restore parts of a single table.
    The restore can be done online.
    If you need to restore only a small subset of data that is either not changed often or where you manually account for consistency this is the way to go.
    This is only available if you enabled mysqldump in the backup configuration (if you have a `mysqldump` directory in the extracted backup the option is available to you).</dd>
</dl>

Determine the option best suited for your needs and continue with the appropriate chapter

## Restore of Mariabackup

To do this we first need to stop the `infra` operator to ensure it does not interfere with our further actions.

After the infra operator is stopped scale the Statefulset of the respective database cluster to 0.
Wait for all database pods to terminate.
For the following guide we assume the name of the Statefulset is `mydbcluster-db`.

Search for the PVCs of the first database pod.
It should be named `data-mydbcluster-db-0`.

### Creating a temporary pod for the restore

To access the PVCs content (and to upload the backup data there) we need to have a pod connected to it.

Run the following command to create a pod for the restore.
Replace `<PVCNAME>` with the name of the PVC from above and `<YOURNAMESPACE>` with the name of the namespaces the cluster is in.

```bash
kubectl -n <YOURNAMESPACE> run restorepod --overrides='
{
    "apiVersion": "v1",
    "kind": "Pod",
    "metadata": {
        "name": "restorepod"
    },
    "spec": {
        "containers": [{
            "command": [
                "sleep",
                "infinity"
            ],
            "image": "alpine:latest",
            "name": "mycontainer",
            "volumeMounts": [{
                "mountPath": "/data",
                "name": "data"
            }]
        }],
        "restartPolicy": "Never",
        "volumes": [{
            "name": "data",
            "persistentVolumeClaim": {
                "claimName": "<PVCNAME>"
            }
        }]
    }
}' --image="alpine:latest"
```

### Restoring the data

Now we need to copy the backup data to the restore pod.
As copying individual files seems quite slow we copy the whole backup `tgz` file.

1. Run `kubectl cp <BACKUP-FILENAME> <YOURNAMESPACE>/restorepod:/tmp`
2. Open a shell inside the `restorepod` and change your directory to `/tmp`
3. Extract the backup using `tar -xf <BACKUP-FILENAME>`
4. Change to the `mariabackup` directory inside the backup
5. Run `cp -R . /data/data/` to restore the backup
6. Remove unneeded logfiles `rm -rf /data/data/ib_logfile*`
7. Close the shell to the `restorepod`
8. Delete the `restorepod`

### Recovering the database

Now we can restart the database to use the restored data.

1. Scale the database statefulset back to 1
2. Observe the database logs to ensure it starts cleanly (Note that Errors regarding the "log sequence number" are fine)
3. If you want you can now connect to the database an verify the restore was successful
4. Restart the `infra` operator. It will scale the database back up to a normal size
5. With this the restore has been completed

## Restore of mysqldump

Decide which mysqldump files you want to restore.
For a single table this will only be the one from that specific table.
Otherwise it might be multiple files or potentially edited files as well.
We will refer to the files you selected as "restore files" below.

1. Copy the restore files to the first database container of the respective database cluster in k8s using `kubectl cp ...`
2. Get the database admin user pw from the secret `<databasename>-**-db-creds` and the key "mariadb-root-password" in there
3. open a shell on the first database pod in the "mariadb-galera" container
4. Load the db dump again using `mysql -u yaook-sys-maint <databasename> -p < restore file` and enter the passwort when prompted (you need to repeat this step for each restore file)
5. With this the restore has been completed
