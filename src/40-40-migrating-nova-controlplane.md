# Migrating Nova Controlplane

The following guide will show you how to migrate the nova controlplane to YAOOK with minimal downtime.

This guide will not handle anything related to compute nodes.
As they are quite correlated with neutron they are covered in a separate guide.

## Downtime Impact

When following this guide you will have a downtime for changes (server creates/deleted, volume attachments) on the nova api depending on the size of your environment.
It is reasonable to finish this migration in 2-3 hours.
Existing servers will work just fine during the whole migration.

In order to make vnc consoles work after the migration you will need to live-/offline-migrate all virtual machines in your environment (so that libvirt picks up the new certificate).

## Requirements

Please ensure the following before following this guide:
* Ensure you have a ready YAOOK setup with operators and their dependencies up and running
* Ensure you have a `KeystoneDeployment` or `ExternalKeystoneDeployment` ready
* Ensure you have at least 3 IPs available for services of type `LoadBalancer`

## Preparations

### Preparations of dependent services

Services like cinder, neutron, heat or octavia rely on the nova endpoint for their operations.
As the internal endpoint of nova will be inside the YAOOK Kubernetes cluster it will no longer be reachable from the outside.
Therefor you might need to change the dependent services to use the `public` endpoint during service discovery.

If you can not change this before the migration, or if you use a static url for nova, you can also do the change during the migration without issues.
This guide will point out when you should change it.

### Prepare a migration policy setting

During the migration you will need to prevent writes to nova.
The easiest way to do this is to update the policy of the service and require admin permissions for all changes.
As the policy differs for each deployment please already prepare such a policy in advance.

### Prepare NovaDeployment

Prepare a `NovaDeployment` resource with the options you want to have set.
You can find documentation for the resource [here](https://docs.yaook.cloud/devel/examples/nova.html).
You can completely ignore the `compute` section for now.

## Migration

A part time of the migration can already be prepared without a downtime.
The downtime start will be explicitly mentioned below.

### Installation of Nova

Installing a parallel nova is already possible if you are careful that the existing keystone endpoint is not overwritten.

1. Stop the keystone-resource-operator by scaling its deployment to 0 and wait for it to terminate
2. Apply the `NovaDeployment` you created above and wait for it to depend on the keystone user
3. Stop the nova-operator by scaling its deployment to 0 and wait for it to terminate
4. Delete the nova and placement endpoint in the `KeystoneEndpoint` resource
5. Start the keystone-resource-operator by scaling its deployment to 1
6. Wait for the nova `KeystoneUser` to reconcile successfully
7. Stop the keystone-resource-operator by scaling its deployment to 0 and wait for it to terminate
8. Start the nova-operator by scaling its deployment to 1
9. Wait for the `NovaDeployment` to reconcile successfully
10. Update the `NovaDeployment` policy setting to have your migration policy from above included
11. Wait for the reconcile to complete

### Making Nova services available externally

The nova-compute services and the neutron-metadata-agent need to be able to connect to internal nova services.
These services are not exposed per default as for a yaook-only cluster they are not needed.
Until all of the nova-compute and neutron services are migrated to yaook you will need to keep these Loadbalancers available.

1. Find the name of the `MysqlService` of nova cell1, it should start with `nova-cell1-`
2. Find the name of the `AmqpServer` of nova cell1, it should start with `nova-cell1-`
3. Create the following 3 services inside your kubernetes cluster
```yaml
---
apiVersion: v1
kind: Service
metadata:
  name: nova-cell1-db-external
  namespace: yaook
spec:
  selector:
    state.yaook.cloud/component: haproxy
    state.yaook.cloud/parent-group: infra.yaook.cloud
    state.yaook.cloud/parent-name: <put-the-nova-cell1-mysqlservice-name-here>
    state.yaook.cloud/parent-plural: mysqlservices
    state.yaook.cloud/parent-version: v1
  type: LoadBalancer
  #loadBalancerIP: 192.168.0.11 # optional, you can also let this be autoassigned
  ports:
  - name: mysql
    port: 3306
    protocol: TCP
    targetPort: 3306
---
apiVersion: v1
kind: Service
metadata:
  name: nova-mq-external
  namespace: yaook
spec:
  selector:
    state.yaook.cloud/component: amqpserver
    state.yaook.cloud/parent-group: infra.yaook.cloud
    state.yaook.cloud/parent-name: <put-the-nova-cell1-amqpserver-name-here>
    state.yaook.cloud/parent-plural: amqpservers
    state.yaook.cloud/parent-version: v1
  type: LoadBalancer
  #loadBalancerIP: 192.168.0.12 # optional, you can also let this be autoassigned
  ports:
  - name: amqps
    port: 5671
    protocol: TCP
    targetPort: 5671
---
apiVersion: v1
kind: Service
metadata:
  name: nova-metadata-external
  namespace: yaook
spec:
  selector:
    state.yaook.cloud/component: nova_metadata
    state.yaook.cloud/parent-group: yaook.cloud
    state.yaook.cloud/parent-name: <put-the-novadeployment-name-here>
    state.yaook.cloud/parent-plural: novadeployments
    state.yaook.cloud/parent-version: v1
  type: LoadBalancer
  #loadBalancerIP: 192.168.0.13 # optional, you can also let this be autoassigned
  ports:
  - name: metadata
    port: 8775
    protocol: TCP
    targetPort: 8775
---
```

### Synchronizing secrets

As the nova controlplane needs to interact with the nova and neutron services outside your cluster some secrets need to be synchronized between them

1. Ensure the secret `nova-metadata-proxy-shared-secret` contains the value your previous nova and neutron services use
2. Trigger the nova-operator once (e.g. by killing it) so that it rolls out this newly set value
3. Wait for the reconcile to complete

### Migration of Nova

Starting here these steps will have the above described Downtime.
Please plan accordingly.

1. Update the nova policy on your existing nova environment to the migration policy defined above. This will prevent further modifications and therefor we can leave the api running.
2. Restart the nova api services afterwards
3. Export the nova api, cell0 and cell1 databases using `mysqldump`
4. To reduce the import time you can delete the shadow tables of nova (used for historic information)
   1. Run the following on the cell1 dump: `sed '/^INSERT INTO `shadow_/d' nova_cell1.dump > nova_cell1.min.dump`
5. For each of the api, cell0 and cell1 databases do the following
   1. Copy the dump to the respective first database container in k8s using "kubectl cp ..."
   2. Get the nova db admin user pw from the secret "nova-<dbname>-**-db-creds" and the key "mariadb-root-password" in there
   3. open a shell on the first nova database pod in the "mariadb-galera" container
   4. Load the db dump again using "mysql -u yaook-sys-maint nova -p < nova_xxx.dump" and enter the passwort when prompted
6. Update the cell mappings in the nova api database
   1. Get the credentials for the `api` user for
      1. cell1 rabbitmq
      2. cell1 mysql
      3. cell0 mysql
   2. Update the `cell_mappings` table in the nova api database:
```
UPDATE cell_mappings SET transport_url="rabbit://api:XXX@nova-cell1-XXX.yaook:5671/" WHERE id=XXX
UPDATE cell_mappings SET database_connection="mysql+pymysql://api:XXX@nova-cell1-XXX.yaook:3306/cell1?charset=utf8&ssl_ca=/etc/ssl/certs/ca-bundle.crt" WHERE id=XXX
UPDATE cell_mappings SET database_connection="mysql+pymysql://api:XXX@nova-cell0-XXX.yaook:3306/cell0?charset=utf8&ssl_ca=/etc/ssl/certs/ca-bundle.crt" WHERE id=XXX
```
7.  If you now `curl` the new nova ingress endpoint you should get a valid response
    - Get yourself a keystone token using `openstack token issue` and save the id
    - in a new shell without ANY OpenStack environment variables set, set the following
        - `OS_TOKEN` to the value of the token from above
        - `OS_ENDPOINT` to the url of nova including the version (e.g. `https://nova.example.com/v2/`)
    - now a `openstack server list` should return the expected result
8.  Start the keystone-resource-operator by scaling its deployment to 1
9.  Wait for the "KeystoneEndpoint" resource to reconcile correctly
10. At this point the yaook `nova` will be active for api requests.

### Updating the neutron metadata agents

As the new nova-metadata service now runs in yaook you will need to update the neutron metadata agents.
Do the following for each of these agents (they are running wherever your l3 and dhcp agents run)

1. Replace the `nova_metadata_host` setting in the `metadata_agent.ini` to point to the above exposed metadata service
2. Restart the `neutron-metadata-agent`

### Updating the compute nodes

The compute nodes need to start using the new nova services in yaook.
Do the following for all of your compute nodes:

1. Set static host entries on the node pointing to the nova rabbitmq (the hostname in the hosts file MUST match the name of the service in k8s)
2. Set static host entries on the node pointing to the nova database (the hostname in the hosts file MUST match the name of the service in k8s)
3. Add the CA certificate of certmanager to the system trusted certificates
4. Configure `[placement] valid_interfaces = public` so that they can talk to placement in yaook
5. Configure nova-compute to use the above exposed rabbitmq and database
6. Restart nova-compute

### Migration of dependent services

As described above you might need to change the configuration of other openstack services to use the new glance api.
If you need to do this, now is the appropriate time.

### Cleaning up

At this point nobody should be using the old nova anymore.
You can validate that on the old nova api logs.
If you still see requests you should clean them up now.

Afterwards:
- Stop the old nova api
- revert your policy to whatever your original setting is

The migration should now be finished.
You can now also clean up everything related to the old nova service.

You will need to keep the `LoadBalancer` services in kubernetes until you migrated all nodes using them

### Fixing vnc

The following needs to be done to make vnc work again.
Do the following for all of your compute nodes:

1. Create a `Certificate` resource in kubernetes similar to the following
```
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: '{{ hostname }}-external-vnc'
  namespace: yaook
spec:
  commonName: '*.{{ hostname }}'
  dnsNames:
  - '*.{{ hostname }}'
  - '*.{{ fqdn }}'
  duration: 1750h0m0s
  issuerRef:
    name: nova-vnc-backend-ca
  renewBefore: 460h0m0s
  secretName: '{{ hostname }}-external-vnc'
  subject:
    organizations:
    - yaook
```
2. Copy the ca certificate, private and public key from the resulting secret to the compute node
3. Configure libvirt to use these certificates for vnc (`vnc_tls***` settings in qemu.conf)

After you did the above for all of your compute nodes you will need to migrate all of your VMs once (live-migration works as well)
