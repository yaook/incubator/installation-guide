Installing the Management Cluster
=================================

- infra-operator
- infra-ironic-operator
- CAs
- monitoring CRDs

.. toctree::
   :maxdepth: 1

   10-10-hardware-requirements.md
   10-20-management-k8s.md
   10-30-svcs.md
   10-60-metal-controller.md
