# Migrating Glance

The following guide will show you how to migrate glance to YAOOK with minimal downtime.

## Downtime Impact

When following this guide you will have a downtime for changes (image uploads, deletes) on the glance api depending on the size of your environment.
It is reasonable to finish this migration in 2-3 hours.
Existing images will work just fine during the whole migration.

## Requirements

Please ensure the following before following this guide:
* Ensure you have a ready YAOOK setup with operators and their dependencies up and running
* Ensure you have a `KeystoneDeployment` or `ExternalKeystoneDeployment` ready
* This migration works if you stay on the same glance storage backend or if you switch it

## Preparations

### Preparataion of keystone

As glance will move during the migration to inside the Kubernetes cluster it might no longer be able to connect to your old keystone endpoints.
At least the connection to the `admin` endpoint is required for token validation.
You might need to change your keystone `admin` endpoint to point to the same url as the `public` endpoint.

### Preparations of dependent services

Services like cinder, nova, heat or octavia rely on the glance endpoint for their operations.
As the internal endpoint of glance will be inside the YAOOK Kubernetes cluster it will no longer be reachable from the outside.
Therefor you might need to change the dependent services to use the `public` endpoint during service discovery.

If you can not change this before the migration, or if you use a static url for glance, you can also do the change during the migration without issues.
This guide will point out when you should change it.

### Prepare a migration policy setting

During the migration you will need to prevent writes to glance.
The easiest way to do this is to update the policy of the service and require admin permissions for all changes.
As the policy differs for each deployment please already prepare such a policy in advance.

### Prepare GlanceDeployment

Prepare a `GlanceDeployment` resource with the options you want to have set.
You can find documentation for the resource [here](https://docs.yaook.cloud/devel/examples/glance.html).

## Migration

A part time of the migration can already be prepared without a downtime.
The downtime start will be explicitly mentioned below.

### Installation of Glance

Installing a parallel glance is already possible if you are careful that the existing keystone endpoint is not overwritten.

1. Stop the keystone-resource-operator by scaling its deployment to 0 and wait for it to terminate
2. Apply the `GlanceDeployment` you created above and wait for it to depend on the keystone user
3. Stop the glance-operator by scaling its deployment to 0 and wait for it to terminate
4. Delete the glance endpoint in the `KeystoneEndpoint` resource
5. Start the keystone-resource-operator by scaling its deployment to 1
6. Wait for the glance `KeystoneUser` to reconcile successfully
7. Stop the keystone-resource-operator by scaling its deployment to 0 and wait for it to terminate
8. Start the glance-operator by scaling its deployment to 1
9. Wait for the `GlanceDeployment` to reconcile successfully
10. Update the `GlanceDeployment` policy setting to have your migration policy from above included
11. Wait for the reconcile to complete

### Pre-Migration of images

If you are switching your storage backend you can already copy over most of your images before your downtime.
If you are not switching your storage backend you can completely skip this step.

The following steps assume you are migrating from ceph to a file storage.
It can probably be adopted to be also usable for cases.

The following steps only work as the glance-api pods still run as root.
Once this is cleaned up you will need a temporary custom pod for this.

1. Open a shell into one of the glance-api pods (use the glance-api container in there)
2. Run the following to install the ceph dependencies `yum install ceph-common -y`
3. Copy the `/etc/ceph/ceph.conf` and an appropriate keyring from an existing node in your environment to `/etc/ceph` inside the container
4. Verify connectivity with `ceph -s`
5. Change to the directory `/var/lib/glance/images/`
6. Run the following command to download all images. Note that the command is idempotent so you can run it multiple times to catch up on changes.
    - Also note that if the command aborts during an export for any reason you MUST delete the exported file as the command will not attempt to continue the download
    - `for i in $(rbd -p images ls); do echo $i; if [ ! -f $i ]; then rbd -p images export $i $i; else echo "already exists"; fi; done`
7. Wait for it to complete. This might take a while

### Migration of Glance

Starting here these steps will have the above described Downtime.
Please plan accordingly.

1. Update the glance policy on your existing glance environment to the migration policy defined above. This will prevent further modifications and therefor we can leave the api running.
2. Restart the glance api services afterwards
3. Export the glance database using `mysqldump`
4. If you migrated your storage backend you will need to update the image paths in the dump
    - You will need to replace `<cephuuid>` with whatever is used in your environment for the ceph connection.
    - WARNING: dirty but sometimes working example ahead:
    - `sed -e 's|rbd://<cephuuid>/images/|file:///var/lib/glance/images/|g' -e 's|/snap||g' glance.dump > glance_cleaned.dump`
5. Copy the dump to the first glance database container in k8s using "kubectl cp ..."
6. Get the glance db admin user pw from the secret "glance-**-db-creds" and the key "mariadb-root-password" in there
7. open a shell on the first glance database pod in the "mariadb-galera" container
8. Load the db dump again using "mysql -u yaook-sys-maint glance -p < glance_cleaned.dump" and enter the passwort when prompted
9. If needed: Run the script for the image pre migration from above again to ensure all images are up to date
10. Do a rolling restart of the glance api containers to clean up the ceph utils and ensure they dont cache anything
11. If you now `curl` the new glance ingress endpoint you should get a valid response
    - Get yourself a keystone token using `openstack token issue` and save the id
    - in a new shell without ANY OpenStack environment variables set, set the following
        - `OS_TOKEN` to the value of the token from above
        - `OS_ENDPOINT` to the url of glance including the version (e.g. `https://glance.example.com/v2/`)
    - now a `openstack image list` should return the expected result
12. Start the keystone-resource-operator by scaling its deployment to 1
13. Wait for the "KeystoneEndpoint" resource to reconcile correctly
14. At this point the yaook `glance` will be active. Creation of volumes and servers from images might still use the old glance

### Migration of dependent services

As described above you might need to change the configuration of other openstack services to use the new glance api.
If you need to do this, now is the appropriate time.

### Cleaning up

At this point nobody should be using the old glance anymore.
You can validate that on the old glance api logs.
If you still see requests you should clean them up now.

Afterwards:
- Stop the old glance api
- revert your policy to whatever your original setting is

The migration should now be finished.
You can now also clean up everything related to the old glance service.
