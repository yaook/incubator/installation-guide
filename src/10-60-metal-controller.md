# Configuring the Services and Installing YAOOK Metal Controller

## Additional NetBox Configuration

Log into the NetBox using its web interface with an admin account.

### Custom fields

The following custom fields are required for proper operation of the Metal Controller:

- `yaook_hostname_prefix` on Device Roles: Text field with `^[a-z]{3}$` regex (regex recommended).
- `yaook_scheduling_keys` on Device Roles: Text field with `^([a-z0-9]([-a-z0-9]*[a-z0-9])?(\.[a-z0-9]([-a-z0-9]*[a-z0-9])?)*/)?[a-z0-9A-Z]([a-z0-9A-Z._-]{0,61}[a-z0-9A-Z])?(:(true|false|strict))(\s*,\s*([a-z0-9]([-a-z0-9]*[a-z0-9])?(\.[a-z0-9]([-a-z0-9]*[a-z0-9])?)*/)?[a-z0-9A-Z]([a-z0-9A-Z._-]{0,61}[a-z0-9A-Z])?(:(true|false|strict)))*$` regex (regex required).
- `yaook_control_plane` on Device Roles: Boolean field
- `yaook_kubernetes_node` on Device Roles: Boolean field. If set to false, yaook/k8s will *not* be installed on the node.
- `yaook_dns_label` on IPAM Roles: Text field with `^([a-z][a-z0-9-]*|\\.)$` regex (regex required)
- `yaook_domain` on Virtualization Clusters: Text field with `^([a-z][a-z0-9-]{0,62}\.)*([a-z][a-z0-9-]{0,62})$` regex (regex required)
- `yaook_bmc_management_method` on `Device Types`: optional. selection of: "redfish", "ipmitool". (ipmitool is optional, we filter just if "redfish" is set. Default is ipmitool.)
- `bond_mode` on `dcim/interfaces`: optional, default 802.3ad, selection of: "802.3ad", "active-backup". Can also add other bond modes (values from here are just passed through)
- `bond_hash` on `dcim/interfaces`: optional, default layer3+4, selection of: "layer3+4", "layer2".
this will translate into transmit-hash-policy within the bonds config

### Tags

In addition, the following tags should be created (the names here are to be put into the *Slug* field when creating the tag -- note that NetBox will override the Slug if you change the name after entering the slug):

- `yaook--default-gateway`: Placed on an IP address, it will be used as default gateway for nodes deployed into the corresponding VLAN/Prefix.
- `yaook--nameserver`: Placed on an IP address, it will be used as nameserver for nodes deployed into the corresponding VLAN/Prefix.
- `yaook--ip-auto-assign`:

    - Placed onto a VLAN, it triggers automatic IP address allocation for any interface which is in that VLAN on nodes which get deployed by the metal controller
    - Placed onto a Prefix, it marks that prefix as source pool for automatically allocated IP addresses

- `yaook--ip-primary`: Placed onto a VLAN, it marks this VLAN as the source for the "primary" IP address of a node. This only has an effect on VLANs with `yaook--ip-auto-assign` set.

## Additional Vault Configuration

Using a Vault identity with sufficient privileges, execute the [`vault-init.sh` script distributed with the metal-controller](https://gitlab.com/yaook/metal-controller/-/blob/devel/utils/vault-init.sh). This script sets up policies and other Vault resources required for the proper usage.

Using an existing (or newly created) approle authentication method, create an approle for the metal controller. **Do not** use the approle created by `vault-init.sh`, as it is reserved for nodes. Assuming the approle is mounted at `auth/common`, you could do the following:

```console
$ vault write auth/common/role/metal-controller token_policies=yaook/group-controller
Success! Data written to: auth/common/role/metal-controller
$ vault read auth/common/role/metal-controller/role-id
Key        Value
---        -----
role_id    2c3c244b-3a38-c549-178c-f4d2bc83ef37
$ vault write -force auth/common/role/metal-controller/secret-id
Key                   Value
---                   -----
secret_id             c69dd78f-4899-d35c-38c1-9929796f9c0c
secret_id_accessor    80eb5b2f-4c50-a25e-082b-240b6054b99a
secret_id_ttl         0s
```

The `yaook/group-controller` policy has been created by `vault-init.sh` and grants the metal controller the minimal privileges necessary to bootstrap nodes and clusters into vault.

**Note:** Even though `vault-init.sh` uses variables for the prefixes, they are hardcoded into the Python side of things, so you cannot really change them.

## Installing the Metal Controller

### Deploying the Metal Controller

**Note:** The below manifest assumes that Vault, NetBox and the Ironic service have been deployed according to this guide. If that is not the case, you'll have to adapt the environment variables set both in the podspec and in the script starting the metal controller to match your requirements.

**Note:** Make sure to substitute `<NETBOX TOKEN>` with the correct token to use for the metal controller.

**Note:** Make sure to substitute `<VAULT *>` with the correct information to authenticate the metal controller. It needs to have at least the privileges of the `yaook/group-controller` policy.

Apply the following manifest after filling in the gaps:

```yaml
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
    name: cluster-repositories
    namespace: yaook
spec:
    accessModes:
    - ReadWriteOnce
    storageClassName: local-storage
    resources:
        requests:
            storage: "12Gi"
---
apiVersion: v1
kind: Service
metadata:
    name: metal-controller
    namespace: yaook
spec:
    clusterIP: None
    selector:
        app.kubernetes.io/name: metal-controller
        app.kubernetes.io/instance: main
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
    name: metal-controller
    namespace: yaook
spec:
    replicas: 1
    serviceName: metal-controller
    selector:
        matchLabels:
            app.kubernetes.io/name: metal-controller
            app.kubernetes.io/instance: main
    template:
        metadata:
            labels:
                app.kubernetes.io/name: metal-controller
                app.kubernetes.io/instance: main
        spec:
            volumes:
              - name: repositories
                persistentVolumeClaim:
                    claimName: cluster-repositories
              - name: vault-cert
                projected:
                    sources:
                      - secret:
                            name: vault-cert
                            items:
                            - key: ca.crt
                              path: ca.crt
            containers:
              - name: metal-controller
                image: registry.yaook.cloud/yaook/metal-controller:devel
                imagePullPolicy: Always
                command:
                  - bash
                  - -euo
                  - pipefail
                  - -c
                  - |
                    export NETBOX_URL="http://$NETBOX_SERVICE_HOST:80/"
                    export OS_BAREMETAL_ENDPOINT="http://$INFRA_IRONIC_SERVICE_HOST:$INFRA_IRONIC_SERVICE_PORT_API/"
                    export OS_BAREMETAL_INTROSPECTION_ENDPOINT="http://$INFRA_IRONIC_INSPECTOR_SERVICE_HOST:$INFRA_IRONIC_INSPECTOR_SERVICE_PORT_API/"
                    export VAULT_ADDR="https://vault.$NAMESPACE.svc.cluster.local:8200/"
                    export VAULT_CAPATH="/mnt/vault-cert/ca.crt"
                    exec /entrypoint.sh metal_controller -vvv
                volumeMounts:
                  - name: repositories
                    mountPath: /var/lib/metal-controller
                  - name: vault-cert
                    mountPath: /mnt/vault-cert
                env:
                  - name: NAMESPACE
                    valueFrom:
                      fieldRef:
                        fieldPath: metadata.namespace
                  - name: NETBOX_TOKEN
                    value: <NETBOX TOKEN>
                  - name: OS_AUTH_TYPE
                    value: none
                  - name: YAOOK_DEPLOY_VAULT_ADDR
                    # this is the vault address under which the deployed *nodes* can reach the vault
                    # note that the vault CA is pushed to the nodes, too (based on VAULT_CAPATH) so HTTPS will work securely out of the box
                    value: https://vault.mgmt.dd1001.cloudandheat.com:32443/
                  - name: YAOOK_DEPLOY_PUBLIC_KEYS
                    # one or more (newline separated) SSH public keys to add to the default user
                    value: <SSH PUBKEY>
                  - name: YAOOK_DEPLOY_IMAGE
                    # URL to the image (must be reachable from within the PXE network; the image server deployed with ironic is typically a good choice)
                    value: <IMAGE URL>
                  - name: YAOOK_DEPLOY_IMAGE_CHECKSUM
                    value: <IMAGE MD5SUM>
                  - name: VAULT_AUTH_PATH
                    value: <APPROLE MOUNTPOINT WITHOUT LEADING auth/>
                  - name: VAULT_ROLE_ID
                    value: <APPROLE ROLE ID>
                  - name: VAULT_SECRET_ID
                    value: <APPROLE SECRET ID>
                  - name: YAOOK_CLUSTER_MAP
                    # This maps the cluster name to a directory in the persistent volume where the cluster repository is held for that cluster.
                    value: |
                      <CLUSTER NAME>: /var/lib/metal-controller/<DIRNAME>/
                  # Set this to true if you want the metal controller to keep the last configdrive generated for a cluster around in the cluster repository.
                  # - name: YAOOK_DEPLOY_CONFIGDRIVE_DEBUG
                  #   value: "true"
                  #
              # If you want to use a dedicated editor of your choice to edit the toml
              # files per cluster (or do general file operations) a sidecar can be used
              # to bring in a container with an integrated editor
              #
              #- name: editor-sidecar
              #  image: <container image path of your editor container>
              #  imagePullPolicy: Always
              #  volumeMounts:
              #    - name: repositories
              #      mountPath: /var/lib/metal-controller
```

In the next section, the bootstrapping and creation of a cluster is discussed.
