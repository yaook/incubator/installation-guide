# Prerequisites

A working YAOOK cluster has some requirements on the surrounding hardware in order to operate and to operate securely.

## Terminology

<dl>
<dt>Cluster Node</dt>
<dd>A <em>cluster node</em> is a node which is part of a YAOOK cluster and not part of the management cluster.</dd>
<dt>Management Node</dt>
<dd>A <em>management node</em> is a node which is part of the management cluster.</dd>
<dt>Control Plane Node</dt>
<dd>A <em>control plane node</em> is a <em>cluster node</em> which is marked as being part of the Kubernetes control plane. That typically means that it has the `master` taint and runs Kubernetes control plane services such as the API server, CNI plugin components and others.</dd>
<dt>Worker Node</dt>
<dd>A <em>worker node</em> is a <em>cluster node</em> which is not a <em>control plane node</em>.</dd>
</dl>

## Management Node Hardware Requirements

Additional requirements are listed in [the Management Cluster Hardware Requirements](./10-10-hardware-requirements.md).

## Cluster Node Hardware Requirements

- REQUIRED: IPMI or comparable interface supported by OpenStack Ironic
- REQUIRED: PXE boot capability
- REQUIRED: UEFI boot capability
- RECOMMENDED: dedicated hardware network interface card for the PXE network

Additional requirements depend on the specific use case for the node. See [Creating a Cluster](./20-00-creating-a-cluster.rst) for details.

## Networking Requirements

- REQUIRED: All Management Nodes need access to the network where the IPMI interfaces of the Cluster Nodes can be reached.
- RECOMMENDED: All Management Nodes need layer 2 access to the network where the PXE interfaces of the Cluster Nodes can be reached.

    - If layer 2 access is not feasible, DHCP forwarding is required so that the nodes can be reliably controlled.

- RECOMMENDED: Port isolation for the PXE interfaces of the Cluster Nodes.

    - This prevents Cluster Nodes from attacking other nodes (including management nodes) via DHCP or ARP spoofing during first boot or during provisioning and thus taking over the cluster.
