# Deploying Nodes

**Note:** For the initial control plane, it is important to not deploy the nodes in parallel. Otherwise, there is a race condition with the bootstrapping of the Kubernetes control plane.

## Deploy a Node

Once the node has been integrated into NetBox, recognized by Ironic and the Metal Controller and IP addresses have been assigned, you should see a planned networking config in the comment field of the node as well as a remark that it is waiting for the node to enter a "deployable" status.

To deploy a node which is in that state, simply change it's status to Active. The metal controller will pick up on the change and initiate the deployment process via Ironic. If any error occurs, it should be logged in the comment field, though it is advisable to also watch the `opensack baremetal node list` output for anything suspicious.

After the node has entered `active` state, depending on the hardware, it usually takes two to ten minutes until it becomes pingable via the IP addresses assigned to it via NetBox. From that point on, the node will automatically install Kubernetes on itself and join or form the cluster.

(NB: the decision on whether to join or form a cluster is made based on the reachability of the API at the IP address configured via `networking_fixed_ip` in cluster configuration step.)

You can follow the on-the-node deploy progress first using `tail -F /var/log/cloud-init-output.log` and once that has settled via `journalctl -fu yaook-deploy` on the node.
