# Configuration Guide

## Key Choices

There are a few key choices you need to make while deploying YAOOK OpenStack. Changing these later can be challenging or even require a reinstallation of components, at least on individual nodes, causing data- or control-plane disruptions.

These choices will be explained in greater detail later in this chapter, if needed. However, we list the key choices here to give you an idea about things you might want to clarify first.

- Which OpenStack release do you want to deploy?

    At the time of writing, Zed and for some services Antelope and Bobcat are
    supported. Note that automatic upgrades are supported from Yoga on.
    All versions before (mainly Train) are deprecated at Yaook. Earlier
    Versions can't get upgraded to Yoga or above.

- Which Neutron multi-layer 2 plugin(s) to use?

    At the time of writing, this is strictly enforced by your OpenStack
    release. Since Yoga you MUST use OVN.

- Which services will run on which nodes?

    This will be discussed in greater detail in the next section, but it is good to mentally pre-classify your nodes at least in two categories: "compute nodes" and "other". Compute nodes are those you want to use to run VMs on, other are everything else.

- How will you connect your deployment to the internet, or comparable upstream networks?

    OpenStack supports different ways of connecting so-called provider networks to your cloud. Provider networks are physical networks offered by the cloud operator, most typically used to provide internet connectivity.

    The different ways are:

    - `flat`: OpenStack expects the provider network to be available as linux interface. Only a single provider network can be handled by this interface.
    - `vlan`: OpenStack expects a linux interface on which it may place zero or more VLAN-separated provider networks. The VLAN IDs are admin-assigned via the OpenStack API.
    - `vxlan` and `gre`: See the upstream documentation for details.

    Depending on your needs, you either need to shape your network layout to match the requirements of the respective provider network type, or you need to choose your provider network type to match what your network can deliver.

    For a simple internet upstream, the `flat` type connected to a border router is generally sufficient.

    For OVN, please also consult the [Reference Architecture](https://docs.openstack.org/neutron/2023.1/admin/ovn/refarch/refarch.html) and related documents.

- Using gateway nodes vs. distributed routing and floating IPs

    The OVN plugin supports two different models: Either you use dedicated gateway nodes (may be colocated with other control plane services) which handle layer 3 address translation and similar services, or you distribute these services on all compute nodes.

    In the latter case, all compute nodes will require access to the provider
    networks according to the previous choice. This is also needed for some
    shared and project specific provider networks (but this are very special
    cases.)

## Labelling and Tainting nodes

Before deploying YAOOK, it is important to label and taint your nodes according to your needs.

Systems deployed by YAOOK in Kubernetes are assigned to nodes based on labels and taints. The exact [set of available labels and taints is fully documented in the reference documentation](https://docs.yaook.cloud/reference/yaook.op.html#yaook.op.common.SchedulingKey). This document describes exactly which services are placed by specific keys.

A scheduling key is a label key in Kubernetes. The label value does not matter for scheduling, but it may be used for configuration purposes. Many components accept multiple scheduling keys. It will run on any node which has either of the scheduling keys set as labels, and it will tolerate its scheduling keys as taints.

### Scheduling guide

The exact labelling and tainting of nodes is dependent on your available hardware and nodes. What follows is a list of "node types" which we recommend you to take into account.

- OpenStack hypervisors: `compute.yaook.cloud/hypervisor`
- Kubernetes control plane: `node-role.kubernetes.io/control-plane` (set by default by kubeadm)
- OpenStack control plane: several labels

    - `any.yaook.cloud/api`
    - `infra.yaook.cloud/any`
    - `operator.yaook.cloud/any`
    - `key-manager.yaook.cloud/barbican-any-service`
    - `block-storage.yaook.cloud/cinder-any-service`
    - `compute.yaook.cloud/nova-any-service`
    - `ceilometer.yaook.cloud/ceilometer-any-service`
    - `key-manager.yaook.cloud/barbican-keystone-listener`
    - `gnocchi.yaook.cloud/metricd`
    - `infra.yaook.cloud/caching`
    - `network.yaook.cloud/neutron-northd`

- OpenStack network nodes:

    - For OVN:

        - `network.yaook.cloud/neutron-ovn-agent`
        - `network.yaook.cloud/neutron-bgp-agent` (if using BGP)

If you run out of nodes, it is recommended to merge the node types "upward". E.g. if you don't have dedicated network nodes, colocate them with the OpenStack control plane.


