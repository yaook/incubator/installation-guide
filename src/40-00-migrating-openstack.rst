Migrating an existing OpenStack Cluster
=======================================

If you are already running an OpenStack Cluster outside of YAOOK and want to migrate it then you can follow the steps in this chapter.
Since each OpenStack environment is different from each other you will need to adopt the procedures for your specific case.

.. toctree::
   :maxdepth: 1

   40-10-migrating-glance.md
   40-20-migrating-keystone.md
   40-30-migrating-cinder.md
   40-40-migrating-nova-controlplane.md
   40-50-migrating-neutron-controlplane.md
